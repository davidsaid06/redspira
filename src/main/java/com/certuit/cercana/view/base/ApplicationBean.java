package com.certuit.cercana.view.base;

import com.certuit.cercana.domain.entity.PerfilUsuario;
import com.certuit.cercana.util.MessageBean;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collection;

public class ApplicationBean implements Serializable {
    FacesContext facesContext;

    @Getter @Setter
    //FIXME: Agregar como configuracion
    private static String destination = "/var/www/html/cercana/files/"; //public
    //private static String destination = "/opt/apache-tomcat-8.5.32/webapps/yaesta/resources/files";
    //private static String destination = "C:/Users/Moises/Desktop/sw-mm-dc-migracion/src/main/webapp/resources/files"; // LOCAL


    @Getter @Setter
    //FIXME: Agregar como configuracion
//  private static String publicUrl = "http://143.202.78.12:8086/almater/files"; //servidor certuit
 //  private static String publicUrl = "http://localhost:8090/resources/files"; // LOCAL
    private static String publicUrl = "http://143.202.78.12:8092/cercana/files/";
    @Getter @Setter
    private static String topicAllDevices = "/topics/allDevices";

    public void setFacesContext(FacesContext facesContext) {
        this.facesContext = facesContext;
    }

    public Object getViewBean(String beanName) {

        return getApplication().createValueBinding(
                "#{viewScope." + beanName + "}").getValue(
                this.getFacesContext());

    }

    public void setViewBean(String beanName, Object value) {

        getApplication().createValueBinding("#{viewScope." + beanName + "}")
                .setValue(this.getFacesContext(), value);

    }

    private Logger log;

    public ApplicationBean() {
        this.log = Logger.getLogger(this.getClass());
    }

    public FacesContext getFacesContext() {

        if (this.facesContext == null)
            return FacesContext.getCurrentInstance();

        return this.facesContext;
    }

    public boolean isValidationFailed() {
        Severity maximumSeverity = FacesContext.getCurrentInstance()
                .getMaximumSeverity();
        boolean validationFailed = false;
        if (maximumSeverity != null
                && (maximumSeverity == FacesMessage.SEVERITY_ERROR || maximumSeverity == FacesMessage.SEVERITY_FATAL)) {
            validationFailed = true;
        }
        return validationFailed;
    }

    public String getBackingBean() {
        FacesContext facesContext = getFacesContext();
        Object result = getApplication().createValueBinding(
                "#{sessionScope.backingBean}").getValue(facesContext);
        return result == null ? "" : result.toString();
    }

    public void setBackingBean(String backingBean) {
        FacesContext facesContext = getFacesContext();
        getApplication().createValueBinding("#{sessionScope.backingBean}")
                .setValue(facesContext, backingBean);
    }

    public Object getRequestBean(String beanName) {
        FacesContext facesContext = getFacesContext();
        return getApplication().createValueBinding(
                "#{requestScope." + beanName + "}").getValue(facesContext);
    }

    public javax.faces.application.Application getApplication() {
        return getFacesContext().getApplication();
    }

    public Object getApplicationBean(String beanName) {
        FacesContext facesContext = getFacesContext();
        return getApplication().createValueBinding(
                "#{applicationScope." + beanName + "}").getValue(facesContext);
    }

    public void setApplicationBean(String beanName, Object value) {
        FacesContext facesContext = getFacesContext();
        getApplication().createValueBinding(
                "#{applicationScope." + beanName + "}").setValue(facesContext,
                value);
    }

    public Object getSessionBean(String beanName) {
        FacesContext facesContext = getFacesContext();
        return getApplication().createValueBinding(
                "#{sessionScope." + beanName + "}").getValue(facesContext);

    }

    public Object getSessionBean(String beanName, Class c) {

        return getApplication()
                .getExpressionFactory()
                .createValueExpression(this.getFacesContext().getELContext(),
                        "#{sessionScope." + beanName + "}", c)
                .getValue(this.getFacesContext().getELContext());
    }

    public void setRequestBean(String beanName, Object value) {
        FacesContext facesContext = getFacesContext();
        getApplication()
                .createValueBinding("#{requestScope." + beanName + "}")
                .setValue(facesContext, value);

    }

    public void setSessionBean(String beanName, Object value) {
        FacesContext facesContext = getFacesContext();
        getApplication()
                .createValueBinding("#{sessionScope." + beanName + "}")
                .setValue(facesContext, value);
    }


    public PerfilUsuario getPerfilUsuario() {
        PerfilUsuario perfilUsuario = (PerfilUsuario) this
                .getSessionBean("perfilUsuario");
        if (perfilUsuario != null)
            return perfilUsuario;
        else {
            return null;
        }
    }

//    public void enviarNotificacion(TipoNotificacion tipoNotificacion, Semaforo semaforo, Object referencia, String contenido, List<Usuario> usuarios) {
//        String titulo = "";
//        int referenciaId = 0;
//        boolean enviado = false;
//        if (referencia != null) {
//            if (referencia instanceof Curso) {
//                Curso curso = (Curso) referencia;
//                titulo = tipoNotificacion.getDescripcion() + ": " + curso.getNombre();
//                referenciaId = curso.getId();
//            }
//            for (Usuario usuario : usuarios) {
//                Notificacion notificacion = new Notificacion(titulo, contenido, referenciaId, semaforo, tipoNotificacion, usuario);
//                try {
//                    this.getServiceLocator().getNotificacionManager().save(notificacion);
//                    enviado = true;
//                } catch (DuplicatedIdException e) {
//                    e.printStackTrace();
//                    enviado = false;
//                }
//            }
//            if (enviado) {
//                this.getFacesContext().addMessage(
//                        null,
//                        new FacesMessage(FacesMessage.SEVERITY_INFO, this
//                                .getAppMessage("message.success"), "Se han enviado notificaciones a los involucrados"));
//            }
//        }
//
//    }

    public boolean hasRole(String role) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }

    public Logger getLog() {
        return log;
    }

    public MessageBean getMessageBean() {
        MessageBean messageBean = (MessageBean) getSessionBean("messageBean");
        if (messageBean == null) {
            messageBean = new MessageBean();
            setSessionBean("messageBean", messageBean);
        }
        return messageBean;
    }

    public String getAppMessage(String key) {
        MessageBean messageBean = (MessageBean) getSessionBean("messageBean");
        if (messageBean == null) {
            messageBean = new MessageBean();
            setSessionBean("messageBean", messageBean);
        }
        return messageBean.getValue(key);
    }



    public String getAppMessage(String key, Object... params) {
        MessageBean messageBean = (MessageBean) getSessionBean("messageBean");
        if (messageBean == null) {
            messageBean = new MessageBean();
            setSessionBean("messageBean", messageBean);
        }
        return MessageFormat.format(messageBean.getValue(key), params);
    }

    public String getAppParameter(String key) {
        MessageBean messageBean = (MessageBean) getSessionBean("messageBean");
        if (messageBean == null) {
            messageBean = new MessageBean();
            setSessionBean("messageBean", messageBean);
        }
        return messageBean.getParam(key);
    }

    public String getHelpMessage(String key) {
        MessageBean messageBean = (MessageBean) getSessionBean("messageBean");
        if (messageBean == null) {
            messageBean = new MessageBean();
            setSessionBean("messageBean", messageBean);
        }
        return messageBean.getHelp(key);
    }

    public String getRealPath(String virtualPath) {
        return this.facesContext.getCurrentInstance()
                .getExternalContext().getRealPath(virtualPath);
    }


}
