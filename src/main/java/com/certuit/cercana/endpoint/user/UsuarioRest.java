package com.certuit.cercana.endpoint.user;

import com.certuit.cercana.domain.entity.Mensaje;
import com.certuit.cercana.domain.entity.Rol;
import com.certuit.cercana.domain.entity.SolicitudContrasena;
import com.certuit.cercana.domain.entity.Usuario;
import com.certuit.cercana.domain.enums.TipoNotificacion;
import com.certuit.cercana.exception.EntityException;
import com.certuit.cercana.exception.UserExistException;
import com.certuit.cercana.exception.UserNotFoundException;
import com.certuit.cercana.service.*;
import com.certuit.cercana.util.JsfUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioRest {

    @Autowired
    private UsuarioService usuarioService;


    @Autowired
    private RolService rolService;

    private static final String PATH_USER_PROFILE = "/profile/images/";
    private static final String TOKEN = "My@874BmKmaQRf-!";

    @GetMapping("/{usuario:.+}")
    public ResponseEntity<Usuario> getUsuario(@PathVariable("usuario") String usuario) {
        Usuario repoUsuario = this.usuarioService.findByUsuario(usuario);
        if (repoUsuario != null) {
            return new ResponseEntity<Usuario>(repoUsuario, HttpStatus.OK);
        } else {
            throw new UserNotFoundException("El usuario " + usuario + " no fue encontrado");
        }
    }

    @PutMapping("/updateDevice/{usuario:.+}")
    @ResponseBody
    public ResponseEntity<Usuario> updateDeviceId(@PathVariable("usuario") String usuario, @RequestParam("deviceId") String deviceId, @RequestParam("deviceType") String deviceType) {
        Usuario repoUsuario = this.usuarioService.findByUsuario(usuario);
        if (repoUsuario != null) {
            repoUsuario.setDeviceId(deviceId);
            repoUsuario.setDeviceType(deviceType);
            repoUsuario.setLastAccess(new Date());
            this.usuarioService.save(repoUsuario);
            return new ResponseEntity<>(repoUsuario, HttpStatus.OK);
        } else {
            throw new UserNotFoundException("El usuario " + usuario + " no fue encontrado");
        }
    }

    @PutMapping("/vincular-facebook/{usuario:.+}")
    @ResponseBody
    public ResponseEntity<Usuario> vincularFacebook(@PathVariable("usuario") String usuario, @RequestParam("facebookId") String facebookId) {
        Usuario repoUsuario = this.usuarioService.findByUsuario(usuario);
        if (repoUsuario != null) {
            Usuario usuarioFacebook = usuarioService.findFirstByFacebookId(facebookId);
            if (usuarioFacebook == null) {
                repoUsuario.setFacebookId(facebookId);
                repoUsuario.setLastAccess(new Date());
                this.usuarioService.save(repoUsuario);
                return new ResponseEntity<>(repoUsuario, HttpStatus.OK);
            } else {
                System.out.println("Ese facebookId: " + facebookId + " ya se encuentra vinculado");
            }
        } else {
            throw new UserNotFoundException("El usuario " + usuario + " no fue encontrado");
        }
        return null;
    }

    @PutMapping("/desvincular-facebook/{usuario:.+}")
    @ResponseBody
    public ResponseEntity<Usuario> desvincularFacebook(@PathVariable("usuario") String usuario) {
        Usuario repoUsuario = this.usuarioService.findByUsuario(usuario);
        if (repoUsuario != null) {
            repoUsuario.setFacebookId(null);
            this.usuarioService.save(repoUsuario);
            return new ResponseEntity<Usuario>(repoUsuario, HttpStatus.OK);
        } else {
            throw new UserNotFoundException("El usuario " + usuario + " no fue encontrado");
        }
    }

    @PutMapping("/updateImageProfile/{usuario:.+}")
    @ResponseBody
    public ResponseEntity<Usuario> updateImageProfile(@PathVariable("usuario") String usuario, @RequestPart MultipartFile img) {
        String urlImage;
        Usuario repoUsuario = this.usuarioService.findByUsuario(usuario);
        if (repoUsuario != null) {
            if (img != null) {
                try {
                    String fileName = img.getOriginalFilename();
                    // extrae la extension del archivo y lo almacehan en la variable
                    img.getOriginalFilename().substring((fileName.lastIndexOf(('.'), fileName.length())));
                    urlImage = JsfUtil.saveFile(fileName, PATH_USER_PROFILE, img.getInputStream());

                    repoUsuario.setImage(urlImage);
                    this.usuarioService.save(repoUsuario);

                    return new ResponseEntity<Usuario>(repoUsuario, HttpStatus.OK);
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new EntityException("usuario-" + usuario);
                }
            }
        }
        throw new UserNotFoundException("El usuario " + usuario + " no fue encontrado");
    }

    @PostMapping("/createUsuario")
    @ResponseBody
    public ResponseEntity<Usuario> createUsuario(@RequestParam("token") String token, @RequestParam("usuario") String email, @RequestParam("contrasena") String contrasena, @RequestParam("tokenDispositivo") String tokenDispositivo,
                                                 @RequestParam("nombre") String nombre, @RequestParam("apellidos") String lastName, @RequestParam("type") String type, @RequestPart(required = false) MultipartFile img, @RequestParam(value = "facebookId", required = false) String facebookId) {
        if (TOKEN.equalsIgnoreCase(token)) {
            Usuario repoUsuario = this.usuarioService.findByUsuario(email);
            if (repoUsuario == null) {
                try {
                    String urlImage = "";
                    List<Rol> roles = new ArrayList<>();
                    Rol rol = this.rolService.findOne(com.certuit.cercana.domain.enums.Rol.USER.getId());
                    roles.add(rol);
                    PasswordEncoder encoder = new BCryptPasswordEncoder(12);
                    String claveAcceso = encoder.encode(contrasena);
                    repoUsuario = new Usuario(email, email, claveAcceso, nombre, nombre, tokenDispositivo, "");
                    repoUsuario.setRoles(roles);
                    repoUsuario.setLastName(lastName);
                    repoUsuario.setFacebookId(facebookId);
                    if (img != null) {
                        String fileName = img.getOriginalFilename();
                        urlImage = JsfUtil.saveFile(fileName, PATH_USER_PROFILE, img.getInputStream());
                        repoUsuario.setImage(urlImage);
                    }
                    repoUsuario.setLastAccess(new Date());
                    Usuario user = this.usuarioService.createUsuario(repoUsuario);
                    return new ResponseEntity<Usuario>(user, HttpStatus.OK);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new EntityException("usuario-" + email);
                }
            } else {
                throw new UserExistException("El usuario " + email + " ya se encuentra registrado");
            }
        } else {
            throw new EntityNotFoundException("usuario-" + email);
        }
    }

    @PutMapping("/updateUsuario/{usuario:.+}")
    @ResponseBody
    public ResponseEntity<Usuario> updateUsuario(@PathVariable("usuario") String usuario, @RequestParam("contrasena") String contrasena, @RequestParam("tokenDispositivo") String tokenDispositivo,
                                                 @RequestParam("nombre") String nombre, @RequestParam("apellidos") String lastName, @RequestParam("type") String type, @RequestParam(value = "facebookId", required = false) String facebookId) {
        Usuario repoUsuario = this.usuarioService.findByUsuario(usuario);
        if (repoUsuario != null) {
            try {
                PasswordEncoder encoder = new BCryptPasswordEncoder(12);
                String claveAcceso = encoder.encode(contrasena);
                repoUsuario.setName(nombre);
                repoUsuario.setEmail(usuario);
                repoUsuario.setDeviceType(type);
                repoUsuario.setDeviceId(tokenDispositivo);
                repoUsuario.setPassword(claveAcceso);
                repoUsuario.setLastName(lastName);
                repoUsuario.setLastAccess(new Date());
                repoUsuario.setFacebookId(facebookId);
                Usuario user = this.usuarioService.createUsuario(repoUsuario);
                return new ResponseEntity<Usuario>(user, HttpStatus.OK);
            } catch (Exception e) {
                e.printStackTrace();
                throw new EntityException("usuario-" + usuario);
            }
        } else {
            throw new UserNotFoundException("El usuario " + usuario + " no fue enontrado");
        }
    }

    @PostMapping("/acceso/{usuario:.+}")
    @ResponseBody
    public boolean getUserPassword(@PathVariable("usuario") String usuario, @RequestParam("password") String password) {
        Usuario repoUsuario = this.usuarioService.findByUsuario(usuario);
        if (repoUsuario != null) {
            PasswordEncoder encoder = new BCryptPasswordEncoder(11);

            if (encoder.matches(password, repoUsuario.getPassword())) {
                System.out.println("Coincide la contraseña");
                return true;
            } else {
                System.out.println("No coincide la contraseña");
                return false;
            }
        }
        throw new UserNotFoundException("El usuario " + usuario + " no fue encontrado");

    }



    @PutMapping("/acceptTerms/{usuario:.+}")
    @ResponseBody
    public ResponseEntity<Usuario> updateUsuario(@PathVariable("usuario") String usuario, @RequestParam("acuerdoFirmado") boolean acuerdoFirmado) {
        Usuario repoUsuario = this.usuarioService.findByUsuario(usuario);
        if (repoUsuario != null) {
            repoUsuario.setAcceptedTerms(acuerdoFirmado);
            this.usuarioService.save(repoUsuario);
            return new ResponseEntity<Usuario>(repoUsuario, HttpStatus.OK);
        } else {
            throw new UserNotFoundException("El usuario " + usuario + " no fue encontrado");
        }
    }

    @GetMapping("/facebookId")
    public ResponseEntity<Usuario> getFacebookId(@RequestParam(value = "facebookId", required = false) String facebookId) {
        if (facebookId == null) {
            return null;
        } else {
            Usuario usuarioFacebook = usuarioService.findFirstByFacebookId(facebookId);
            if (usuarioFacebook != null) {
                return new ResponseEntity<Usuario>(usuarioFacebook, HttpStatus.OK);
            } else {
                throw new UserNotFoundException("FacebookId " + facebookId + " no fue encontrado");
            }
        }
    }




}
