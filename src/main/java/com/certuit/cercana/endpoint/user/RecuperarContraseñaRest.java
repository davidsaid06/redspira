package com.certuit.cercana.endpoint.user;

import com.certuit.cercana.domain.entity.Mensaje;
import com.certuit.cercana.domain.entity.SolicitudContrasena;
import com.certuit.cercana.domain.entity.Usuario;
import com.certuit.cercana.domain.enums.TipoNotificacion;
import com.certuit.cercana.exception.UserNotFoundException;
import com.certuit.cercana.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;

@RestController
@RequestMapping("/api")
public class RecuperarContraseñaRest {

    @Autowired
    private ParametroSistemaService parametroSistemaService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private SolicitudContrasenaService solicitudContrasenaService;
    @Autowired
    private MensajeService mensajeService;

    @PostMapping("/send-recover-password")
    @ResponseBody
    public ResponseEntity<Usuario> sendPasswordEmail(@RequestParam("email") String email) {
        Usuario repoUsuario = this.usuarioService.findByUsuario(email);
        if (repoUsuario != null) {

            SolicitudContrasena solicitud = new SolicitudContrasena();
            Calendar c = Calendar.getInstance();
            Date fechaSolicitud = c.getTime();
            c.add(Calendar.DATE, 1);
            Date fechaExpiracion = c.getTime();

            solicitud.setUsuario(repoUsuario);
            solicitud.setAtendida(false);
            solicitud.setFechaExpiracion(fechaExpiracion);
            solicitud.setFechaSolicitud(fechaSolicitud);

            PasswordEncoder encoder = new BCryptPasswordEncoder(11);
            String token = encoder.encode("" + System.currentTimeMillis());
            solicitud.setLlave(token);

            try {
                this.solicitudContrasenaService.save(solicitud);
                this.enviarCorreo(repoUsuario, token);
            } catch (Exception e) {

                e.printStackTrace();
                return null;
            }
            return new ResponseEntity<>(repoUsuario, HttpStatus.OK);
        } else {
            throw new UserNotFoundException("El usuario " + email + " no fue encontrado");
        }
    }

    private void enviarCorreo(Usuario usuario, String token) {
        Mensaje mensajeCorreo = mensajeService.getMensajeByTipoNotificacion(TipoNotificacion.RecuperarContraseña.getId());

        if (mensajeCorreo != null) {
            String mensaje = mensajeCorreo.getContenido().replace("@nombre", usuario.getName());
            String url = parametroSistemaService.getPublicServerUrl() + "views/restablecer-contrasena.xhtml?token=" + token;
            mensaje = mensaje.replace("@url", url);
            mensaje = mensaje.replace("@token", token);
            mensaje = mensaje.replace("@email", usuario.getEmail());
            emailService.sendMail(mensaje, mensajeCorreo.getAsunto(), usuario.getEmail());
//            correoEnviado = true;
        }
    }
}
