package com.certuit.cercana.endpoint.Redspira;


import com.certuit.cercana.domain.entity.CatalogoParametro;
import com.certuit.cercana.service.Redspira.CatalogoParametroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MonitorRest {

    @Autowired
    private CatalogoParametroService parametroService;




    @GetMapping("/get-params")
    public List<CatalogoParametro> findAll() {
        return parametroService.findAll();
    }





}
