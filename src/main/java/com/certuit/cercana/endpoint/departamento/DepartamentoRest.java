package com.certuit.cercana.endpoint.departamento;

import com.certuit.cercana.domain.entity.Departamento;
import com.certuit.cercana.repositories.DepartamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DepartamentoRest {

    @Autowired
    private DepartamentoRepository departamentoRepo;

    @GetMapping("/departamentos")
    public List<Departamento> getDepartamentos() { return departamentoRepo.findAll(); }
    
    @GetMapping("/departamentos/{id}")
    public Departamento getCalifID(@PathVariable Integer id) {
        return departamentoRepo.findOne(id);
    }

    @PostMapping("/departamentos")
    @ResponseBody
    public ResponseEntity<Departamento> postDepartamento(@RequestParam("owners") Integer owners,
                                                         @RequestParam("name") String name,
                                                         @RequestParam("description") String description,
                                                         @RequestParam("status") String status) {

        Departamento nuevo = new Departamento(owners,name,description,status);

        if (nuevo != null) {
            return new ResponseEntity<Departamento>(departamentoRepo.save(nuevo), HttpStatus.OK);
        }
        throw new EntityNotFoundException("Error"); //TODO poner un error real
    }

    @PutMapping("/departamentos")
    @ResponseBody
    public ResponseEntity<Departamento> putDepartamento(@RequestParam("id") Integer id,
                                                        @RequestParam("owners") Integer owners,
                                                        @RequestParam("name") String name,
                                                        @RequestParam("description") String description,
                                                        @RequestParam("status") String status) {
        Departamento departamentoModificado = departamentoRepo.findAllById(id);

        if (departamentoModificado != null) {
            departamentoModificado.updateDepartamento(owners,name,description,status);
            return new ResponseEntity<Departamento>(departamentoRepo.save(departamentoModificado), HttpStatus.OK);
        }
        throw new EntityNotFoundException("Error no se ha podido encontrar el departamento con el ID - "+id);
    }

    @DeleteMapping("/departamentos/{id}")
    @ResponseBody
    public ResponseEntity<Departamento> deleteDepartamento(@PathVariable("id") Integer DepartamentoID) {
        Departamento userDepartamento = departamentoRepo.findAllById(DepartamentoID);
        if (userDepartamento != null){
            departamentoRepo.delete(userDepartamento);
            System.out.println();
            return new ResponseEntity<Departamento>(userDepartamento,HttpStatus.OK);
        }
        throw new EntityNotFoundException("Error no se ha podido encontrar el departamento con el ID - "+DepartamentoID);
    }

}
