package com.certuit.cercana.endpoint.ticket;

import com.certuit.cercana.domain.dto.DataNotification;
import com.certuit.cercana.domain.dto.Notification;
import com.certuit.cercana.domain.dto.Push;
import com.certuit.cercana.domain.entity.ActividadTicket;
import com.certuit.cercana.domain.entity.Departamento;
import com.certuit.cercana.domain.entity.Ticket;
import com.certuit.cercana.domain.entity.Usuario;
import com.certuit.cercana.domain.enums.ClickAction;
import com.certuit.cercana.domain.enums.EstatusTicket;
import com.certuit.cercana.domain.enums.TipoAccionTicket;
import com.certuit.cercana.repositories.DepartamentoRepository;
import com.certuit.cercana.service.PushNotificationService;
import com.certuit.cercana.service.RolService;
import com.certuit.cercana.service.UsuarioService;
import com.certuit.cercana.service.staff.StaffService;
import com.certuit.cercana.service.ticket.ActividadTicketService;
import com.certuit.cercana.service.ticket.TicketService;
import com.certuit.cercana.util.FirebaseResponse;
import com.certuit.cercana.util.JsfUtil;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TicketRest {

    private static final String PATH_IMG = "tickets/images/";
    @Autowired
    private TicketService ticketService;
    @Autowired
    private DepartamentoRepository departamentoRepository;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private StaffService staffService;
    @Autowired
    private ActividadTicketService actividadTicketService;
    @Autowired
    private PushNotificationService pushNotificationService;
    @Autowired
    private RolService rolService;

    @Setter
    private DataNotification dataNotification = new DataNotification();



    @GetMapping("/tickets")
    public List<Ticket> getTickets() {
        return ticketService.getTickets();
    }

    @GetMapping("/tickets/seen")
    public List<Ticket> getViewedTickets() {
        return ticketService.getNewTickets();
    }

    @GetMapping("/tickets/staff/{id}")
    public List<Ticket> getTicketsStaffMember(@PathVariable("id") Integer id) {
        return ticketService.getTicketsStaffMember(id);
    }

    @GetMapping("/tickets/user/{id}")
    public List<Ticket> getTicketsUserMember(@PathVariable("id") Integer id) {
        return ticketService.getTicketsByAuthor(id);
    }

    @GetMapping("/tickets/{id}")
    public Ticket getTicket(@PathVariable Integer id) {
        return ticketService.getTicketID(id);
    }

    @PostMapping("/tickets/create")
    @ResponseBody
    public ResponseEntity<Ticket> postTicket(@RequestParam("title") String title,
                                             @RequestParam("content") String content,
                                             @RequestParam(value = "language", required = false) String language,
                                             @RequestParam(value = "priority", required = false) String priority,
                                             @RequestParam(value = "file", required = false) MultipartFile file,
                                             @RequestParam("department_id") Integer department_id,
                                             @RequestParam("author_id") Integer author_id,
                                             @RequestParam(value = "owner_id", required = false) Integer owner_id,
                                             @RequestParam(value = "location", required = false) String location,
                                             @RequestParam(value = "lat", required = false) String lat,
                                             @RequestParam(value = "lon", required = false) String lon,
                                             @RequestParam(value = "locationDescription", required = false) String location_description) {
        Usuario staff = null;
        try {
        String urlImage = "";
        if (owner_id != null)
            staff = this.staffService.findById(owner_id);
        Usuario usuario = this.usuarioService.findOne(author_id);
        String tnumber = JsfUtil.getFormatNextTicket(this.ticketService.getMaxTicketNumber());

        ticketService.getMaxTicketNumber();
        Ticket nuevo = ticketService.postTicket(tnumber, true, priority, true, title, content, language, new Date(),
                false, department_id, usuario, staff, location,
                lat, lon, location_description, null, 0, EstatusTicket.NUEVO.getNombre(), file);



        if (nuevo != null) {
            FirebaseResponse responseEntity;
            Notification notification = new Notification("default", "Nuevo ticket: " + nuevo.getTicketNumber(), nuevo.getContent(), ClickAction.OPEN_TICKET_ACTIVITY.name());
            notification.setId(nuevo.getId());
            dataNotification.setId(nuevo.getId());
            dataNotification.setClickAction(ClickAction.OPEN_TICKET_ACTIVITY.name());

            List<Usuario> staffs = staffService.getByDepartamentoAndNivel(nuevo.getDepartment().getId(), 3);
            for (Usuario user: staffs) {
                Push push = new Push(user.getDeviceId(), "high", notification, dataNotification);
                responseEntity = pushNotificationService.sendNotification(push);
                System.err.println("success: " + responseEntity.getSuccess());
                System.err.println("faiulure: " + responseEntity.getFailure());
                System.err.println("Results: " + responseEntity.getResults());
            }

            return new ResponseEntity<Ticket>(nuevo, HttpStatus.OK);
        }
        }catch (Exception e){
            e.printStackTrace();
            throw new EntityNotFoundException("Error al enviar la imagen");
        }
        throw new EntityNotFoundException("Error"); //TODO poner un error real
    }

    @PutMapping("/tickets/update")
    @ResponseBody
    public ResponseEntity<Ticket> putTicket(@RequestParam("id") Integer id,
                                            @RequestParam("ticket_number") String tnumber,
                                            @RequestParam("unread") boolean unread,
                                            @RequestParam("priority") String priority,
                                            @RequestParam("unread_staff") boolean unread_staff,
                                            @RequestParam("title") String title,
                                            @RequestParam("content") String content,
                                            @RequestParam("language") String language,
                                            @RequestParam("file") String file,
                                            @RequestParam("date") Date date,
                                            @RequestParam("closed") boolean closed,
                                            @RequestParam("author_email") String author_email,
                                            @RequestParam("author_name") String author_name,
                                            @RequestParam("department_id") Integer department_id,
                                            @RequestParam("author_id") Integer author_id,
                                            @RequestParam("owner_id") Integer owner_id,
                                            @RequestParam("location") String location,
                                            @RequestParam("lat") String lat,
                                            @RequestParam("lon") String lon,
                                            @RequestParam("locationDescription") String location_description,
                                            @RequestParam("closed_date") Date closed_date,
                                            @RequestParam("calification") Integer calification,
                                            @RequestParam("estatus") String estatus) {

        Usuario staff = this.staffService.findById(owner_id);
        Usuario usuario = this.usuarioService.findOne(author_id);
        Ticket ticketModificada = ticketService.putTicket(id, tnumber, unread, priority, unread_staff, title, content, language, file, date,
                closed, author_email, author_name, department_id, author_id, owner_id, location,
                lat, lon, location_description, closed_date, calification, estatus);

        if (ticketModificada != null) {
            Departamento departamento = this.departamentoRepository.findAllById(department_id);
            ticketModificada.setNuevosValores(tnumber, unread, priority, unread_staff, title, content, language, date, closed, departamento, usuario, staff, location, lat, lon, location_description, closed_date, calification, estatus);
            return new ResponseEntity<Ticket>(ticketModificada, HttpStatus.OK);
        }
        throw new EntityNotFoundException("Error no se ha podido encontrar el ticket con el ID - " + id);
    }

    @PutMapping("/tickets/{id}/change-department/{new}")
    @ResponseBody
    public ResponseEntity<Ticket> ticketChangeDepartment(@PathVariable("id") Integer id,
                                                         @PathVariable("new") Integer newDepa) {
        Ticket ticketModificada = ticketService.ticketChangeDepartment(id, newDepa);
        if (ticketModificada != null) {
            return new ResponseEntity<Ticket>(ticketModificada, HttpStatus.OK);
        }
        throw new EntityNotFoundException("Error no se ha podido encontrar el ticket con el ID - " + id);
    }

    @PutMapping("/tickets/{id}/assign-staff/{staff}")
    @ResponseBody
    public ResponseEntity<Ticket> ticketAssignStaff(@PathVariable("id") Integer id,
                                                         @PathVariable("staff") Integer staffId) {
        Ticket ticket = ticketService.assignTicket(staffId, id);
        if (ticket != null) {

            return new ResponseEntity<>(ticket, HttpStatus.OK);
        }
        throw new EntityNotFoundException("Error no se ha podido encontrar el ticket con el ID - " + id);

    }

    @GetMapping("/tickets/search-tickets")
    public List<Ticket> busquedaGlobal(@RequestParam(value = "query", required = true) String ticketNumber) {

        return ticketService.getBusquedaGlobal(ticketNumber);
    }

    @PutMapping("/tickets/close/{id}")
    @ResponseBody
    public ResponseEntity<Ticket> ticketClose(@PathVariable("id") Integer id) {
        Ticket ticketModificada = ticketService.ticketClose(id);
        if (ticketModificada != null) {
            return new ResponseEntity<Ticket>(ticketModificada, HttpStatus.OK);
        }
        throw new EntityNotFoundException("Error no se ha podido encontrar un ticket abierto con el ID - " + id);
    }

    @PutMapping("/tickets/{id}/qualify/{calif}")
    @ResponseBody
    public ResponseEntity<Ticket> ticketQualify(@PathVariable("id") Integer id,
                                                @PathVariable("calif") Integer calif) {
        Ticket ticketModificada = ticketService.ticketQualify(id, calif);
        if (ticketModificada != null) {
            return new ResponseEntity<Ticket>(ticketModificada, HttpStatus.OK);
        }
        throw new EntityNotFoundException("Error no se ha podido encontrar el ticket con el ID - " + id);
    }
    @PutMapping("/tickets/{id}/comment")
    @ResponseBody
    public ResponseEntity<ActividadTicket> ticketComment(@PathVariable("id") Integer id,@RequestParam("content") String comment,@RequestParam("user_id") Integer userId,
                                                @RequestPart(value = "file", required = false) MultipartFile file) {

        Ticket ticket = ticketService.findOne(id);

        ActividadTicket actividadTicket = new ActividadTicket();

        Date date = new Date();
        String urlImage = "";
        actividadTicket.setTicket(ticket);
        actividadTicket.setTipo(0);
        actividadTicket.setTitulo("Test");
        actividadTicket.setMensaje(comment);
        actividadTicket.setFechaActividad(date);


        Usuario user = usuarioService.findOne(userId);
        actividadTicket.setUsuario(user);

        FirebaseResponse responseEntity;
        Notification notification = new Notification("default","Nueva respuesta de ticket: " + ticket.getTicketNumber() ,actividadTicket.getMensaje() , ClickAction.OPEN_TICKET_ACTIVITY.name());
        notification.setId(ticket.getId());
        dataNotification.setId(ticket.getId());
        dataNotification.setClickAction(ClickAction.OPEN_TICKET_ACTIVITY.name());
        if (ticket.getUsuario().getId() == userId){
            if (ticket.getStaff() != null) {
                Push push = new Push(ticket.getStaff().getDeviceId(), "high", notification, dataNotification);
                responseEntity = pushNotificationService.sendNotification(push);
                System.err.println("success: " + responseEntity.getSuccess());
                System.err.println("faiulure: " + responseEntity.getFailure());
                System.err.println("Results: " + responseEntity.getResults());

            }
        }else {
            Push push = new Push(ticket.getUsuario().getDeviceId(), "high", notification, dataNotification);
            responseEntity = pushNotificationService.sendNotification(push);
            System.err.println("success: " + responseEntity.getSuccess());
            System.err.println("faiulure: " + responseEntity.getFailure());
            System.err.println("Results: " + responseEntity.getResults());

        }



        actividadTicket.setType(TipoAccionTicket.COMMENT.getDescripcion());

        try {
            if (file != null) {
                String fileName = file.getOriginalFilename();
                // extrae la extension del archivo y lo almacena en la variable
                file.getOriginalFilename().substring((fileName.lastIndexOf(('.'), fileName.length())));
                urlImage = JsfUtil.saveFile(fileName, PATH_IMG, file.getInputStream());
                actividadTicket.setFile(urlImage);
            }
            actividadTicketService.save(actividadTicket);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }


        return new ResponseEntity<>(actividadTicket, HttpStatus.OK);
    }

    @DeleteMapping("/tickets/{id}")
    @ResponseBody
    public ResponseEntity<Ticket> deleteTicket(@PathVariable("id") Integer TicketID) {
        Ticket userTicket = ticketService.deleteTicket(TicketID);
        try {
            if (userTicket != null) {
                return new ResponseEntity<Ticket>(userTicket, HttpStatus.OK);
            }
            throw new EntityNotFoundException("Error no se ha podido encontrar el ticket con el ID - " + TicketID);
        } catch(EntityNotFoundException ex) {
            return new ResponseEntity<Ticket>(userTicket, HttpStatus.NOT_FOUND);
        }

    }

}
