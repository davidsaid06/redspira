package com.certuit.cercana.config;

import com.certuit.cercana.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private UsuarioService myAppUserDetailsService;

//    @Autowired
//    private CustomAccessDeniedHandler accessDeniedHandler;

    @Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;

    @Override
    protected void configure(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.
                jdbcAuthentication()
                .usersByUsernameQuery(usersQuery)
                .authoritiesByUsernameQuery(rolesQuery)
                .dataSource(dataSource)
                .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
//                .antMatchers("/api/**").hasAuthority("PACIENTE")
                .antMatchers("/**").permitAll()
                .antMatchers("/").permitAll()
                .antMatchers("/views/restablecer-contrasena.xhtml").permitAll()
                .antMatchers("/oauth/token/**").permitAll()
                .antMatchers("/javax.faces.resource/**").permitAll()
                .antMatchers("/views/login.*").permitAll()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/oauth/token").permitAll()
//                .antMatchers("/views/admin/**").hasAuthority("ADMIN")
                .antMatchers("/views/admin/**").hasAnyAuthority("ADMIN", "ADMINISTRATIVO", "MEDICO")
                .antMatchers("/views/common/**").authenticated()
//                .antMatchers("/api/auth/**").permitAll()
                .antMatchers("/api/usuarios/createUsuario").permitAll()
                .antMatchers("/api/usuarios/facebookId").permitAll()
                .antMatchers("/api/acuerdos/uso").permitAll()
                .antMatchers("/api/estados").permitAll()
                .antMatchers("/api/send-recover-password").permitAll()

                //TODO NO PERMITIR A TODOS ENTRAR
                .antMatchers("/api/userId/*").permitAll()
                .antMatchers("/api/recoleccion/**").permitAll()
                .antMatchers("/api/tickets/**").permitAll()
                .antMatchers("/api/ticket/**").permitAll()
                .antMatchers("/api/tickets").permitAll()
                .antMatchers("/api/tickets").permitAll()
                .antMatchers("/api/tickets/**").permitAll()
                .antMatchers("/api/staff/**").permitAll()
                .antMatchers("/api/staff").permitAll()
                .antMatchers("/api/departamentos").permitAll()
                .antMatchers("/api/departamentos/**").permitAll()
                .antMatchers("/api/departamentos").permitAll()
                .antMatchers("/api/articulo/**").permitAll()
                .antMatchers("/api/articulos").permitAll()
                //.antMatchers("/api/articulos").permitAll()
                .antMatchers("/api/system/**").permitAll()
                .antMatchers("/api/articulos/**").permitAll()
                .antMatchers("/api/userId/*").permitAll()
                //TODO NO PERMITIR A TODOS ENTRAR

                .anyRequest()
                .authenticated()
                .and().httpBasic()
                .and().csrf().disable().formLogin()
                .loginPage("/cercana/views/login.html")// yaesta/views/login.xhtml
                .defaultSuccessUrl("/views/admin/dashboard.xhtml")
                .usernameParameter("username")
                .passwordParameter("password")
                .and().logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/").and().exceptionHandling()
                .accessDeniedPage("/error/access.xhtml");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/error/**", "/javax.faces.resource/**");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myAppUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

}