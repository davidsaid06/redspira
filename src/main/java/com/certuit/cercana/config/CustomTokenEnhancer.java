package com.certuit.cercana.config;

import com.certuit.cercana.domain.entity.Rol;
import com.certuit.cercana.domain.entity.Usuario;
import com.certuit.cercana.service.UsuarioService;
import com.certuit.cercana.service.staff.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;


public class CustomTokenEnhancer implements TokenEnhancer {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private StaffService staffService;


    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
        if (oAuth2Authentication.getPrincipal() instanceof User) {
            User user = (User) oAuth2Authentication.getPrincipal();
            Usuario usuario = usuarioService.findByUsuario(user.getUsername());
            additionalInfo.put("name", usuario.getName());
            additionalInfo.put("lastName", usuario.getLastName());
            additionalInfo.put("fullName", usuario.getFullName());
            additionalInfo.put("image", usuario.getImage());
            additionalInfo.put("username", user.getUsername());
            additionalInfo.put("authorities", user.getAuthorities());
            boolean isStaff = false;
            boolean isUser = false;
            for (Rol rol : usuario.getRoles()) {
                if (rol.getId() == com.certuit.cercana.domain.enums.Rol.USER.getId()) {
                    isUser = true;
                } else {
                    isStaff = true;
                }
            }
            additionalInfo.put("isStaff", isStaff);
            additionalInfo.put("isUser", isUser);
        }
        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(additionalInfo);
        return oAuth2AccessToken;
    }

}
