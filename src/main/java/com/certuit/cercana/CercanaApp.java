package com.certuit.cercana;

import com.certuit.cercana.util.Cron;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@Configuration
@SpringBootApplication
@EnableAsync
@EnableScheduling
public class CercanaApp extends SpringBootServletInitializer {


    public static void main(String[] args) {
        Cron cron = new Cron();
        SpringApplication.run(CercanaApp.class, args);
        //cron.async();
    }


    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    private static Class<CercanaApp> applicationClass = CercanaApp.class;

}
