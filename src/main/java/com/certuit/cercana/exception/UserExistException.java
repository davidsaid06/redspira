package com.certuit.cercana.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserExistException extends RuntimeException {

    @Getter
    private String errorMessage = "Usuario proporcionado ya existente";
    public UserExistException(String exception) {
        super(exception);
    }

}