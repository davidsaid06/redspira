package com.certuit.cercana.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class EntityException extends RuntimeException {

    @Getter
    private String errorMessage = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();

    public EntityException(String exception) {
        super(exception);
    }

}