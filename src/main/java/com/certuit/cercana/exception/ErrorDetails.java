package com.certuit.cercana.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class ErrorDetails {
    private Date timestamp;
    private String message;
    private String details;
    private String error;

    public ErrorDetails(Date timestamp, String message, String details, String error) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
        this.error = error;
    }

}