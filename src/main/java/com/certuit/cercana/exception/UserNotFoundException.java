package com.certuit.cercana.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

    @Getter
    private String errorMessage = HttpStatus.NOT_FOUND.getReasonPhrase();
    public UserNotFoundException(String exception) {
        super(exception);
    }

}