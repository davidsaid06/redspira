package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@EntityListeners(AuditListener.class)
public class ParametroSistema extends EntityBase implements Auditable {

    private String propiedad;
    private String valor;

    public ParametroSistema() {
    }
}
