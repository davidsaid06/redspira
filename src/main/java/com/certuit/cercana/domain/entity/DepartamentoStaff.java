package com.certuit.cercana.domain.entity;


import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "department_staff")
@EntityListeners(AuditListener.class)
public class DepartamentoStaff extends EntityBase implements Auditable, Serializable {

    @ManyToOne
    @JoinColumn(name = "department_id", nullable = false)
    private Departamento department;
    @ManyToOne
    @JoinColumn(name = "staff_id", referencedColumnName = "id", nullable = false)
    private Usuario staff;
    private boolean enabled;


    public DepartamentoStaff() {
    }

}
