package com.certuit.cercana.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
@Entity
@Table(name = "area_monitor")
public class AreaMonitor implements Serializable {

    @Id
    @Column(name = "idarea")
    @Getter @Setter private int id;

    @Column(name = "idmonitor")
    @Getter @Setter private String idMonitor;
}
