package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
public class RespuestaRapida extends EntityBase implements Auditable, Serializable {

    private String nombre;
    private String contenido;
    private boolean favorito;
    private boolean vigente;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RespuestaRapida respuestaRapida = (RespuestaRapida) o;
        return id == respuestaRapida.id &&
                Objects.equals(nombre, respuestaRapida.nombre) &&
                Objects.equals(contenido, respuestaRapida.contenido) &&
                Objects.equals(favorito, respuestaRapida.favorito) &&
                Objects.equals(vigente, respuestaRapida.vigente);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, contenido, favorito, vigente);
    }

}
