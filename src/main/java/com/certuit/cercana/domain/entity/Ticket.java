package com.certuit.cercana.domain.entity;


import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import com.certuit.cercana.domain.enums.EstatusTicket;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.crypto.Data;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "ticket")
@EntityListeners(AuditListener.class)
public class Ticket extends EntityBase implements Auditable, Serializable {

    private String ticketNumber;
    private String priority;
    private String title;
    private String content;
    private String language;
    private String image;

    // ESTATUS
    //FIXME manage Status Enum
    private String estatus;
    private Date date;
    private boolean unread;
    private boolean unreadStaff;
    private boolean closed;
    private Date closedDate;
    private String calification;

    @ManyToOne
    @JoinColumn(name = "department_id", nullable = false)
    private Departamento department;
    @ManyToOne
    @JoinColumn(name = "author_id",  nullable = false)
    private Usuario usuario;
    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = true)
    private Usuario staff;

    // LOCATION
    private String location; //Nullable
    private String lat; //Nullable
    private String lon; //Nullable
    private String locationDescription; //Nullable


    //    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "ticket_id")
    @Transient
    @JsonManagedReference
    private List<ActividadTicket> events; //Nullable

    public Ticket() {
    }

    public Ticket(String ticket_number, boolean unread, String priority, boolean unread_staff, String title, String content, String language, Date date, boolean closed, Departamento department_id, Usuario author_id, Usuario owner, String location, String lat, String lon, String location_description, Date closed_date, Integer calification, String estatus) {
        this.ticketNumber = ticket_number;
        this.priority = priority;
        this.title = title;
        this.content = content;
        this.language = language;
        this.estatus = estatus;
        this.date = date;
        this.unread = unread;
        this.unreadStaff = unread_staff;
        this.closed = closed;
        this.closedDate = closed_date;
        this.calification = String.valueOf(calification);
        this.department = department_id;
        this.usuario = author_id;
        this.staff = owner;
        this.location = location;
        this.lat = lat;
        this.lon = lon;
        this.locationDescription = location_description;
    }

    public void setNuevosValores(String ticket_number, boolean unread, String priority, boolean unread_staff, String title, String content, String language, Date date, boolean closed, Departamento department, Usuario author_id, Usuario owner_id, String location, String lat, String lon, String location_description, Date closed_date, Integer calification, String estatus) {
        this.ticketNumber = ticket_number;
        this.unread = unread;
        this.priority = priority;
        this.unreadStaff = unread_staff;
        this.title = title;
        this.content = content;
        this.language = language;
        this.date = date;
        this.closed = closed;
        this.department = department;
        this.usuario = author_id;
        this.staff = owner_id;
        this.location = location;
        this.lat = lat;
        this.lon = lon;
        this.locationDescription = location_description;
        this.closedDate = closed_date;
        this.calification = String.valueOf(calification);
        this.estatus = estatus;
    }

    public void commentTicket(String comment, Data data) {

    }

    public void setReadUnread(boolean unread) {
        this.unread = unread;
    }

    public void setClosed(boolean closed, Date closed_date) {
        this.closed = closed;
        setReadUnread(true);
        if (closed) {
            this.estatus = EstatusTicket.RESUELTO.getNombre();
        } else {
            if (this.staff != null) {
                this.estatus = EstatusTicket.ASIGNADO.getNombre();
            } else {
                this.estatus = EstatusTicket.POR_ASIGNAR.getNombre();
            }
        }
        if (this.closedDate == null) {
            this.closedDate = closed_date;
        }
    }

    public void changeDepartment(Departamento department) {
        this.department = department;
        this.title = department.getName();
        setReadUnread(true);
        Boolean removeStaff = true;
        if (staff != null) {
            for (int x = 0; x < staff.getDepartments().size(); x++) {
                if (staff.getDepartments().get(x).getId() == this.department.getId()) {
                    removeStaff = false;
                }
            }
            if (removeStaff) {
                this.staff = null;
            }
        }

        if (this.staff != null) {
            this.estatus = EstatusTicket.ASIGNADO.getNombre();
        } else {
            this.estatus = EstatusTicket.POR_ASIGNAR.getNombre();
        }
    }

    public void setCalification(Integer calification) {
        this.calification = String.valueOf(calification);
    }

    public boolean getClosedStatus() {
        return this.closed;
    }

//    public Date closedSince() {
//        String str = this.closed_date.toString().replace(".", "").substring(0, 12);
//        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMddHHmm");
//        Date date1 = new Date();
//        try {
//            date1 = formatter1.parse(str);
//        } catch (Exception e) {
//        }
//        return date1;
//    }

    public void assignTicket(Usuario staff) {
        setReadUnread(true);
        this.staff = staff;
        this.estatus = EstatusTicket.ASIGNADO.getNombre();
    }

    public void staffReadTicket(Boolean read) {
        this.unreadStaff = read;
    }

    public EstatusTicket getEstatusTicket() {
        return EstatusTicket.getValue(estatus);
    }


}
