package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.enums.TipoActividadTicket;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Entity
@Setter
@Getter
@Table(name = "comentarios")
public class ActividadTicket {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "author_user_id", referencedColumnName = "id", nullable = true)
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "ticket_id", nullable = true)
    @JsonBackReference
    private Ticket ticket;

    @Column(name = "content")
    private String mensaje;

    @Column (name = "fecha_actividad", columnDefinition = "TIMESTAMP")
    private Date fechaActividad;

    @Column(name = "type")
    private String type;

    @Column(name = "file")
    private String file;

    //ADD ELEMENTS

    private int tipo;
    private String titulo;

    @Transient
    private String imageURL;

    public ActividadTicket() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActividadTicket paciente = (ActividadTicket) o;
        return id == paciente.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public TipoActividadTicket getTipoActividadTicket() {
        return TipoActividadTicket.getValue(type);
    }

    public String getShortLongDateFormat(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy hh:mm:ss a");
        return  sdf.format(date);
    }



}
