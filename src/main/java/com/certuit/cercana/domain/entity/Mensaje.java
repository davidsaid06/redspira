package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import com.certuit.cercana.domain.enums.TipoNotificacion;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "mensaje")
@EntityListeners(AuditListener.class)
public class Mensaje extends EntityBase implements Auditable {

    @Column(name = "contenido", length = 2000)
    private String contenido;
    private String nombre;
    @Column(name = "tipo")
    private int tipoNotificacion;
    private String asunto;

    public Mensaje() {
        super();
    }

    public Mensaje(int id, String contenido, String nombre) {
        super();
        this.id = id;
        this.contenido = contenido;
        this.nombre = nombre;
    }

    public Mensaje(String contenido, String asunto) {
        super();
        this.contenido = contenido;
        this.asunto = asunto;
    }

    public TipoNotificacion getTipoNotificacion() {
        return TipoNotificacion.valueOf(this.tipoNotificacion);
    }

    public void setTipoNotificacion(TipoNotificacion tipo) {
        this.tipoNotificacion = tipo.getId();
    }
}