package com.certuit.cercana.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
@Entity
@Table(name = "cat_area")
public class CatalogoArea implements Serializable {

    @Id
    @Column(name = "idarea")
    @Getter @Setter private int idArea;

    @Column(name = "descarea")
    @Getter @Setter private String descripcion;

//    @Column(columnDefinition="geom")
 //   @Type(type="org.hibernate.spatial.GeometryType")    //"org.hibernatespatial.GeometryUserType" seems to be for older versions of Hibernate Spatial
 //   public com.vividsolutions.jts.geom.Point geom;
}
