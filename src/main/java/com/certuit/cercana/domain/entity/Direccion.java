package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@Table(name = "track_user_address")
public class Direccion extends EntityBase implements Auditable, Serializable {

    private String label;
    private Double lat;
    private Double lng;
    //FIXME Identify functionality of that
    private int zoneId;
    private int userId;
    private boolean notification_e;
    private boolean notificationEnabled;

    public Direccion(Double lat, Double lng, String label, Boolean noti, int zone_id, int user_id) {
        this.lat = lat;
        this.lng = lng;
        this.label = label;
        this.notificationEnabled = noti;
        this.zoneId = zone_id;
        this.userId = user_id;
    }

    public Direccion(Double lat, Double lng, String label, Boolean noti,
                     int zone_id, Boolean notification_e, int user_id) {
        this.lat = lat;
        this.lng = lng;
        this.label = label;
        this.notificationEnabled = noti;
        this.zoneId = zone_id;
        this.notification_e = notification_e;
        this.userId = user_id;
    }

    public Direccion() {
    }

    public void setNuevosValores(Double lat, Double lng, String label, Boolean noti,
                                 int zone_id, Boolean notification_e, int user_id) {
        this.lat = lat;
        this.lng = lng;
        this.label = label;
        this.notificationEnabled = noti;
        this.zoneId = zone_id;
        this.notification_e = notification_e;
        this.userId = user_id;
    }
}
