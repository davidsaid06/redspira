package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.Audit;
import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.awt.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "track_zone")
@EntityListeners(AuditListener.class)
public class Ruta extends EntityBase implements Auditable {

    private int noRuta;
    private String address;
    private String label;
    private int radius;
    private String type;
    private Polygon geom;
    private String observaciones;
    private Integer sustituto;
    private int status; // 0- No stat, 1- OK, 2- NOT OK
    private String sector;
    //FIXME check this
    private Timestamp fechaRestauracion;


    @OneToOne()
    @JoinColumn(name = "zoneId")
    private HorarioRuta horario;

    @OneToOne()
    @JoinColumn(name = "truck_id")
    private Camion camion;

    public Ruta() {
    }

    public Ruta(String address, String label, int radius, String type, Polygon geom) {
        this.address = address;
        this.label = label;
        this.radius = radius;
        this.type = type;
        this.geom = geom;
    }

    public void setNuevosValores(String address, String label, int radius, String type, Polygon geom) {
        this.address = address;
        this.label = label;
        this.radius = radius;
        this.type = type;
        this.geom = geom;
    }

    public void changeStatus(int status) {
        this.status = status;
    }

    public void agregarObservaciones(String observaciones, Integer sustituto, Timestamp date) {
        this.observaciones = observaciones;
        this.sustituto = sustituto;
        this.fechaRestauracion = date;
    }

    @Override
    public Audit getAudit() {
        return null;
    }

    @Override
    public void setAudit(Audit audit) {

    }
}
