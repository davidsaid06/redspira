package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
public class Acuerdo extends EntityBase implements Auditable {

    private String tipo;
    private String nombre;
    private String contenido;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Acuerdo acuerdo = (Acuerdo) o;
        return id == acuerdo.id &&
                Objects.equals(tipo, acuerdo.tipo) &&
                Objects.equals(nombre, acuerdo.nombre) &&
                Objects.equals(contenido, acuerdo.contenido);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tipo, nombre, contenido);
    }
}
