package com.certuit.cercana.domain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;


public class PerfilUsuario implements Serializable {

	@Getter
    @Setter
	private int usuarioId;
	@Getter
    @Setter
	private String nombreCompleto;
	@Getter
    @Setter
	private String usuario;
	@Getter
    @Setter
	private String correoElectronico;
	@Getter
	@Setter
	private String urlImagen;
	@Getter
    @Setter
	private List<Rol> roles;
	@Getter
    @Setter
	private boolean activo;
	@Getter
    @Setter
	private boolean superUsuario;


	public PerfilUsuario(Usuario usuario) {
		this.setUsuarioId(usuario.getId());
		this.usuario = usuario.getUsername();
		this.nombreCompleto= usuario.getName();
		this.correoElectronico= usuario.getEmail();
		this.roles = usuario.getRoles();
		this.activo = usuario.isEnabled();
		this.urlImagen = usuario.getImage();
	}

	public PerfilUsuario(Usuario usuario, List<Rol> roles) {
		this.setUsuarioId(usuario.getId());
		this.usuario = usuario.getUsername();
		this.nombreCompleto= usuario.getName();
		this.correoElectronico= usuario.getEmail();
		this.roles = usuario.getRoles();
		this.activo = usuario.isEnabled();
	}

	public boolean hasRole(String role) {
		boolean hasRole = false;
		for (Rol authority : this.roles) {
			hasRole = authority.getNombre().equals(role);
			if (hasRole) {
				break;
			}
		}
		return hasRole;
	}
}
