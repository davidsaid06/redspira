package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.Audit;
import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@Table(name = "department")
public class Departamento extends EntityBase implements Auditable, Serializable {

    //FIXME Check Owner maybe could be a User
    private int owner;
    private String name;
    private String description;
    private String status;
    private boolean enabled;

    public Departamento() {
    }

    public Departamento(int owners, String name, String description, String status) {
        this.owner = owners;
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public void updateDepartamento(int owners, String name, String description, String status) {
        this.owner = owners;
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public Audit getAudit() {
        return null;
    }

    @Override
    public void setAudit(Audit audit) {

    }
}
