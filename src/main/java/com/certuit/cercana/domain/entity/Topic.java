package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
public class Topic extends EntityBase implements Auditable, Serializable {

    private String name;
    private String icon;
    private String iconColor;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Topic acuerdo = (Topic) o;
        return id == acuerdo.id &&
                Objects.equals(name, acuerdo.name) &&
                Objects.equals(icon, acuerdo.icon);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, icon);
    }
}
