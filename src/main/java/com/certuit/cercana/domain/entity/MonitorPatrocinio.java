package com.certuit.cercana.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
@Entity
@Table(name = "monitor_patrocinio")
public class MonitorPatrocinio implements Serializable {

    @Id
    @Column(name = "idpatrocinio")
    @Getter @Setter private String id;

    @Column(name = "descpatrocinio")
    @Getter @Setter private String descripcion;

    @Column(name = "logo")
    @Getter @Setter private String logo;

    @Column(name = "enlace")
    @Getter @Setter private String enlace;

}
