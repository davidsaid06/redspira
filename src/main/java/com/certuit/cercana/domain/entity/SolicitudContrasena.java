package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
public class SolicitudContrasena extends EntityBase implements Auditable {

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario;
    private String llave;
    @Column(columnDefinition = "TIMESTAMP")
    private Date fechaSolicitud;
    @Column(columnDefinition = "TIMESTAMP")
    private Date fechaExpiracion;
    private boolean atendida;


}
