package com.certuit.cercana.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
@Entity
@Table(name = "monitor")
public class Monitor implements Serializable {

    @Id
    @Column(name = "idmonitor")
    @Getter @Setter private String id;

    @Column(name = "descmonitor")
    @Getter @Setter private String descripcion;

    @Column(name = "x")
    @Getter @Setter private float x;

    @Column(name = "y")
    @Getter @Setter private float y;

    @Column(name = "tasa")
    @Getter @Setter private Double tasa;

    @Column(name = "fechainstalacion")
    @Getter @Setter private Date fechaInstalacion;

    @Column(name = "idpatrocinio")
    @Getter @Setter private int idPatrocinador;

    @Column(name = "tipo")
    @Getter @Setter private String tipo;

    @Column(name = "ultima_actualizacion")
    @Getter @Setter private Date ultimaActualizacion;

    @Column(name = "activo")
    @Getter @Setter private boolean activo;
}
