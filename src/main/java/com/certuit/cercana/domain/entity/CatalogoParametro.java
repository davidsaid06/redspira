package com.certuit.cercana.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
@Entity
@Table(name = "cat_param")
public class CatalogoParametro implements Serializable {

    @Id
    @Column(name = "parameterid")
    @Getter @Setter private String id;

    @Column(name = "nom_param")
    @Getter @Setter private String nombre;

    @Column(name = "unidad")
    @Getter @Setter private String unidad;

    @Column(name = "aqi")
    @Getter @Setter private boolean aqi;

}
