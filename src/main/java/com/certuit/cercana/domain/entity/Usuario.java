package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@Table(name = "usuario")
public class Usuario extends EntityBase implements Auditable, UserDetails, Serializable {

    private String username;
    private String email;
    private String password;
    private String name;
    private String lastName;
    private String image;
    private String deviceId;
    //FIXME correct value for deviceType
    private String deviceType;
    private String facebookId;
    @Column(columnDefinition = "TIMESTAMP")
    private Date lastAccess;
    private boolean enabled;
    private boolean acceptedTerms;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usuario_has_rol",
            joinColumns = {@JoinColumn(name = "usuario_id")},
            inverseJoinColumns = {@JoinColumn(name = "rol_id")}
    )
    private List<Rol> roles;

    private int level;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "department_staff",
            joinColumns = @JoinColumn(name = "staff_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "department_id", referencedColumnName = "id"))
    private List<Departamento> departments;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities =
                new ArrayList<GrantedAuthority>();
        for (Rol rol : getRoles()) {
            authorities.add(new SimpleGrantedAuthority(rol.getNombre()));
        }
        return authorities;
    }

    public Usuario() {
    }

    public Usuario(String username, String email, String password, String name, String lastName, String deviceId, String deviceType) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.name = name;
        this.lastName = lastName;
        this.deviceId = deviceId;
        this.deviceType = deviceType;
        this.enabled = true;
        this.acceptedTerms = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return id == usuario.id &&
                Objects.equals(username, usuario.username) &&
                Objects.equals(password, usuario.password) &&
                Objects.equals(lastName, usuario.lastName) &&
                Objects.equals(email, usuario.email) &&
                Objects.equals(deviceId, usuario.deviceId) &&
                Objects.equals(name, usuario.name);
    }

    public String getFullName() {
        return this.name + " " + this.lastName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, password, name);
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean hasRole(String role) {
        boolean hasRole = false;
        for (GrantedAuthority authority : this.getAuthorities()) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }

    public boolean hasAnyRole(String authorities) {
        List<String> roles = Arrays.asList(authorities.split(","));
        for (String role : roles) {
            if (this.hasRole(role.trim()))
                return true;
        }
        return false;
    }
}
