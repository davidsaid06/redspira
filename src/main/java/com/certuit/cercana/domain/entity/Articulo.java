package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@Table(name = "article")
public class Articulo extends EntityBase implements Auditable, Serializable {

    private String title;
    private String content;
    private int position;
    @ManyToOne
    @JoinColumn(name = "topic_id", nullable = false)
    private Topic topic;
    @Column(columnDefinition = "TIMESTAMP")
    private Date lastEdited;
    private String author;
    private String emailAuthor;
    private boolean enabled;

    public Articulo() {

    }

    public Articulo(String title, String content, Date lastEdited, Integer position, Topic topic, String author, String emailAuthor) {
        this.title = title;
        this.content = content;
        this.lastEdited = lastEdited;
        this.position = position;
        this.topic = topic;
        this.author = author;
        this.emailAuthor = emailAuthor;
        this.enabled = true;
    }

    public void modificarArticulo(String title, String content, Date last_edited, Integer position, Topic topic_id, String nombre_creador_art, String email_creador_art) {
        this.title = title;
        this.content = content;
        this.lastEdited = last_edited;
        this.position = position;
        this.topic = topic_id;
        this.author = nombre_creador_art;
        this.emailAuthor = email_creador_art;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Articulo articulo = (Articulo) o;
        return id == articulo.id &&
                Objects.equals(topic, articulo.topic);
    }
}
