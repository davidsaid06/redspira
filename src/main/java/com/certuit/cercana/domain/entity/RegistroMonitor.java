package com.certuit.cercana.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement
@Entity
@Table(name = "registro_monitor")
public class RegistroMonitor implements Serializable {

    @Id
    @Column(name = "stationid")
    @Getter @Setter private String idEstacion;

    @Column(name = "parameterid")
    @Getter @Setter private String idParametro;

    @Column(name = "date")
    @Getter @Setter private Date fecha;

    @Column(name = "hr")
    @Getter @Setter private int hora;

    @Column(name = "val")
    @Getter @Setter private Double valor;

    @Column(name = "x")
    @Getter @Setter private Double x;

    @Column(name = "y")
    @Getter @Setter private Double y;

    @Column(name = "datetime")
    @Getter @Setter private Date tiempo;


}
