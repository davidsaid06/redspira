package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "track_zone_date")
@EntityListeners(AuditListener.class)
public class HorarioRuta extends EntityBase implements Auditable {

    private String trackerId;
    private int d1;
    private int d2;
    private int turno;
    private String horaEntrada;
    private String horaSalida;
    /* Turnos
     * 1 = "4:00AM-11:00AM"
     * 2 = "8:00AM-16:00PM"
     * 3 = "13:00PM-10:00PM"
     * */

    public HorarioRuta() {
    }

    public HorarioRuta(String tracker_id, int d1, int d2, int turno, String horaEntrada, String horaSalida) {
        this.trackerId = tracker_id;
        this.d1 = d1;
        this.d2 = d2;
        this.turno = turno;
        this.horaEntrada = horaEntrada;
        this.horaSalida = horaSalida;
    }

    public void setNuevosValores(String tracker_id, int d1, int d2, int turno, String horaEntrada, String horaSalida) {
        this.trackerId = tracker_id;
        this.d1 = d1;
        this.d2 = d2;
        this.turno = turno;
        this.horaEntrada = horaEntrada;
        this.horaSalida = horaSalida;
    }
}
