package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@Table(name = "track_rating_zone")
public class CalificacionRuta extends EntityBase implements Auditable, Serializable {

    private String calification;
    private int zoneId;
    private int userId;
    private Date date;

    public CalificacionRuta(String calification, int zone_id, int user_id, Date date) {
        this.calification = calification;
        this.zoneId = zone_id;
        this.userId = user_id;
        this.date = date;
    }

    public CalificacionRuta() {
    }

    public void setCalificacion(String nueva) {
        this.date = new Date();
        this.calification = nueva;
    }
}
