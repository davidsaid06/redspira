package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Setter
@Getter
@Table(name = "notification")
@EntityListeners(AuditListener.class)
public class Notificacion extends EntityBase implements Auditable {
    @ManyToOne
    @JoinColumn(name = "usuario_id", referencedColumnName = "id", nullable = false)
    private Usuario usuario;
    private String titulo;
    private String mensaje;
    //FIXME check this
    @Column(columnDefinition = "TIMESTAMP")
    private Date fechaCreacion;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notificacion notificacion = (Notificacion) o;
        return id == notificacion.id &&
                Objects.equals(usuario, notificacion.usuario) &&
                Objects.equals(titulo, notificacion.titulo) &&
                Objects.equals(mensaje, notificacion.mensaje) &&
                Objects.equals(fechaCreacion, notificacion.fechaCreacion);

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, titulo, mensaje, fechaCreacion);
    }
}
