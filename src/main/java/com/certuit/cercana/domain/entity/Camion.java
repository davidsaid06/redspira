package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@EntityListeners(AuditListener.class)
@Table(name = "trucks")
public class Camion extends EntityBase implements Auditable, Serializable {

    //    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private int numeroCamion;
    private boolean camionApoyo;

    public Camion() {
    }

    public Camion(int numero_camion, boolean apoyo) {
        this.numeroCamion = numero_camion;
        this.camionApoyo = apoyo;
    }

    public void setNuevosValores(int numero_camion, boolean apoyo) {
        this.numeroCamion = numero_camion;
        this.camionApoyo = apoyo;
    }
}
