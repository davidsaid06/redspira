package com.certuit.cercana.domain.entity;

import com.certuit.cercana.domain.base.AuditListener;
import com.certuit.cercana.domain.base.Auditable;
import com.certuit.cercana.domain.base.EntityBase;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "track_zone_log")
@EntityListeners(AuditListener.class)
public class RutaObservaciones extends EntityBase implements Auditable {

    private int noRuta;
    private Integer noCamionOriginal;
    private Integer noCamionSustituto;
    private String observaciones;
    private int status;
    private int turno;
    private String sector;
    private Timestamp fechaReporte;
    private Timestamp fechaRestauracion;

    public RutaObservaciones() {
    }

    public RutaObservaciones(int noRuta, Integer noCamionOriginal, Integer noCamionSustituto, String observaciones,
                             int status, int turno, String sector, Timestamp fechaReporte, Timestamp fechaRestauracion) {
        this.noRuta = noRuta;
        this.noCamionOriginal = noCamionOriginal;
        this.noCamionSustituto = noCamionSustituto;
        this.observaciones = observaciones;
        this.status = status;
        this.turno = turno;
        this.sector = sector;
        this.fechaReporte = fechaReporte;
        this.fechaRestauracion = fechaRestauracion;
    }
}
