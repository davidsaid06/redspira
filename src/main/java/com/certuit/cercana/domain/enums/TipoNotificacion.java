package com.certuit.cercana.domain.enums;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public enum TipoNotificacion {
	RecuperarContraseña(1, "RecuperarContraseña"), AsignarUsuario(2, "Asignar Usuario");;
	
	@Getter
    @Setter
    int id;
	@Getter
    @Setter
    String descripcion;
	
	private TipoNotificacion(int id) {
		this.id = id;
	}
	
	private TipoNotificacion(int id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}
	
	public static TipoNotificacion valueOf(int id) {
		switch (id) {
			case 1:
				return RecuperarContraseña;
			case 2:
				return AsignarUsuario;
			default:
				return RecuperarContraseña;
		}
	}
	
	public static List<TipoNotificacion> getAll() {
		List<TipoNotificacion> tiposNotificacion = new ArrayList<TipoNotificacion>();
		for (TipoNotificacion tipoNotificacion : TipoNotificacion.values())
			tiposNotificacion.add(tipoNotificacion);
		return tiposNotificacion;
	}
}