package com.certuit.cercana.domain.enums;

import lombok.Getter;
import lombok.Setter;

public enum CategoriaActividadUsuario {
    GENERAL(1, "General", "fa fa-asterisk"),
    PROGRAMAS(2, "Programas", "fa fa-heartbeat"),
    PROMOCIONES(3, "Promociones", "fa fa-tag"),
    SERVICIOS(4, "Servicios", "fa fa-ambulance"),
    MEDICOS(5, "Medicos", "fa fa-userId-md"),
    CITAS(6, "Citas", "fa fa-calendar"),
    CONVENIOS(7, "Convenios", "fa far fa-handshake"),
    ASISTENTE_SEGUROS(8, "Asistente personal en seguros de gastos médicos", "fas fa-userId-shield"),
    FACTURACION(9, "Facturación", "fas fa-file-invoice-dollar"),
    AGENDAS(10, "Agendas", "fa far fa-calendar"),
    COMPLEJOS(11, "Complejos", "fa fa-hospital-o"),
    ESPECIALIDADES(12, "Especialidades", "fa fa-stethoscope"),
    ETAPAS_GESTACION(13, "Etapas de gestación", "fa fa fa-spinner"),
    RESPONSABLES(14, "Responsables", "fa fa-male"),
    TIPO_CITAS(15, "Tipos de cita", "fa fal fa-list-alt"),
    RESPUESTAS_RAPIDAS(16, "Respuestas rápidas", "fa fa-flash"),
    PREGUNTAS_FRECUENTES(17, "Preguntas frecuentes", "fa-question-circle"),
    USUARIOS(18, "Usuarios", "fa fa-userId"),
    ACUERDOS(19, "Acuerdos", "fa fa-exclamation-circle"),
    BENEFICIOS(20, "Beneficios", "fa fa-heart");


    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String nombre;
    @Getter
    @Setter
    private String icono;

    private CategoriaActividadUsuario(int id) {
        this.id = id;
    }

    private CategoriaActividadUsuario(int id, String nombre, String icono) {
        this.id = id;
        this.nombre = nombre;
        this.icono = icono;

    }

    public static CategoriaActividadUsuario valueOf(int id) {
        switch (id) {
            case 1:
                return GENERAL;
            case 2:
                return PROGRAMAS;
            case 3:
                return PROMOCIONES;
            case 4:
                return SERVICIOS;
            case 5:
                return MEDICOS;
            case 6:
                return CITAS;
            case 7:
                return CONVENIOS;
            case 8:
                return ASISTENTE_SEGUROS;
            case 9:
                return FACTURACION;
            case 10:
                return AGENDAS;
            case 11:
                return COMPLEJOS;
            case 12:
                return ESPECIALIDADES;
            case 13:
                return ETAPAS_GESTACION;
            case 14:
                return RESPONSABLES;
            case 15:
                return TIPO_CITAS;
            case 16:
                return RESPUESTAS_RAPIDAS;
            case 17:
                return PREGUNTAS_FRECUENTES;
            case 18:
                return USUARIOS;
            case 19:
                return ACUERDOS;
            case 20:
                return BENEFICIOS;
            default:
                return GENERAL;
        }
    }
}

