package com.certuit.cercana.domain.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * Rol enum.
 *
 * @author VMontes
 */

public enum Rol {
    ADMIN(1, "Administrador", 5),
    USER(2, "Ciudadano", 0),
    OPERATOR(3, "Operador", 1),
    SUPERVISOR(4, "Supervisor", 2),
    DEPARTMENT(5, "Jefe Departamento", 3),
    DIRECTOR(6, "Director General", 4),
    RECOLECTOR(7, "Recolector", 6),
    Indistinto(-1, "No detectado", 0);

    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String nombre;
    @Getter
    @Setter
    private int nivel;

    Rol(int id, String nombre, int nivel) {
        this.id = id;
        this.nombre = nombre;
        this.nivel = nivel;
    }

    public static Rol valueOf(int id) {
        switch (id) {
            case -1:
                return Indistinto;
            case 1:
                return ADMIN;
            case 2:
                return USER;
            case 3:
                return OPERATOR;
            case 4:
                return SUPERVISOR;
            case 5:
                return DEPARTMENT;
            case 6:
                return DIRECTOR;
            case 7:
                return RECOLECTOR;
            default:
                return Indistinto;
        }
    }
}
