package com.certuit.cercana.domain.enums;

import lombok.Getter;
import lombok.Setter;

public enum ClickAction {
    OPEN_TICKET_ACTIVITY(1, "OPEN_TICKET_ACTIVITY"),
    OPEN_NOTICE_ACTIVITY(2, "OPEN_NOTICE_ACTIVITY");

    @Getter
    @Setter
    int id;
    @Getter
    @Setter
    String descripcion;

    private ClickAction(int id) {
        this.id = id;
    }

    private ClickAction(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public static ClickAction valueOf(int id) {
        switch (id) {
            case 1:
                return OPEN_TICKET_ACTIVITY;
            default:
                return OPEN_NOTICE_ACTIVITY;
        }
    }
}