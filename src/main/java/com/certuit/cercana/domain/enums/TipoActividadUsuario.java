package com.certuit.cercana.domain.enums;

import lombok.Getter;
import lombok.Setter;

public enum TipoActividadUsuario {
    CONTESTO(1, "Contestó"),
    CREO(2, "Creó"),
    MODIFICO(3, "Modificó"),
    ELIMINO(4, "Eliminó"),
    DESACTIVO(5, "Deshabilitó"),
    ACTIVO(6, "Habilitó"),
    AGENDO(7, "Agendó"),
    RESTABLECIO(8, "Restableció"),
    DEFAULT(9, "");


    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String nombre;

    private TipoActividadUsuario(int id) {
        this.id = id;
    }

    private TipoActividadUsuario(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;

    }

    public static TipoActividadUsuario valueOf(int id) {
        switch (id) {
            case 1:
                return CONTESTO;
            case 2:
                return CREO;
            case 3:
                return MODIFICO;
            case 4:
                return ELIMINO;
            case 5:
                return DESACTIVO;
            case 6:
                return ACTIVO;
            case 7:
                return AGENDO;
            case 8:
                return RESTABLECIO;
            default:
                return DEFAULT;
        }
    }
}

