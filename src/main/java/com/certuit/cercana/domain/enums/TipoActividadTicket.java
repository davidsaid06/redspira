package com.certuit.cercana.domain.enums;

import lombok.Getter;
import lombok.Setter;

public enum TipoActividadTicket {
    INTERNA(1, "Interna"),
    SISTEMA(2, "Sistema"),
    USUARIO(3, "Usuario");

    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String nombre;

    private TipoActividadTicket(int id) {
        this.id = id;
    }

    private TipoActividadTicket(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;

    }

    public static TipoActividadTicket getValue(String value) {
        switch (value) {
            case "ASSIGN":
                return INTERNA;
            case "COMMENT":
                return USUARIO;
            case "CLOSE":
                return INTERNA;
            case "UN_ASSIGN":
                return INTERNA;
            case "RE_OPEN":
                return INTERNA;
            case "PRIORITY_CHANGED":
                return INTERNA;
            case "DEPARTMENT_CHANGED":
                return INTERNA;
            default:
                return SISTEMA;
        }

    }

    public static TipoActividadTicket valueOf(int id) {
        switch (id) {
            case 1:
                return INTERNA;
            case 2:
                return SISTEMA;
            case 3:
                return USUARIO;
            default:
                return USUARIO;
        }
    }
}

