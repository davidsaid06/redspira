package com.certuit.cercana.domain.enums;

import lombok.Getter;
import lombok.Setter;

public enum TipoAccionTicket {
    ASSIGN(1, "ASSIGN"),
    COMMENT(2, "COMMENT"),
    CLOSE(3, "CLOSE"),
    UN_ASSIGN(4, "UN_ASSIGN"),
    RE_OPEN(5, "RE_OPEN"),
    PRIORITY_CHANGED(6, "PRIORITY_CHANGED"),
    DEPARTMENT_CHANGED(7, "DEPARTMENT_CHANGED");

    @Getter
    @Setter
    int id;
    @Getter
    @Setter
    String descripcion;

    private TipoAccionTicket(int id) {
        this.id = id;
    }

    private TipoAccionTicket(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public static TipoAccionTicket valueOf(int id) {
        switch (id) {
            case 1:
                return ASSIGN;
            case 2:
                return COMMENT;
            case 3:
                return CLOSE;
            case 4:
                return UN_ASSIGN;
            case 5:
                return RE_OPEN;
            case 6:
                return PRIORITY_CHANGED;
            case 7:
                return DEPARTMENT_CHANGED;
            default:
                return ASSIGN;
        }
    }

}