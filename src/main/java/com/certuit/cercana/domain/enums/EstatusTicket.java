package com.certuit.cercana.domain.enums;

import lombok.Getter;
import lombok.Setter;

public enum EstatusTicket {
    NUEVO(1, "Nuevo"),
    ASIGNADO(2, "Asignado"),
    RESUELTO(3,"Resuelto"),
    RESPUESTA_NUEVA_CIUDADANO(4, "Respuesta nueva Ciudadano"),
    PROCESO(5, "Proceso"),
    RESPUESTA_NUEVA_STAFF(6, "Respuesta nueva staff"),
    CERRADO(7, "cerrado"),
    POR_ASIGNAR(8, "Por Asignar");

    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String nombre;

    private EstatusTicket(int id) {
        this.id = id;
    }

    private EstatusTicket(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;

    }

    public static EstatusTicket valueOf(int id) {
        switch (id) {
            case 1:
                return NUEVO;
            case 2:
                return ASIGNADO;
            case 3:
                return RESUELTO;
            case 4:
                return RESPUESTA_NUEVA_CIUDADANO;
            case 5:
                return PROCESO;
            case 6:
                return RESPUESTA_NUEVA_STAFF;
            case 7:
                return CERRADO;
            case 8:
                return RESUELTO;
            default:
                return POR_ASIGNAR;
        }
    }

    public static EstatusTicket getValue(String search) {
        for (EstatusTicket each : EstatusTicket.values()) {
            if (each.nombre.compareToIgnoreCase(search) == 0) {
                return each;
            }
        }
        return null;
    }
}

