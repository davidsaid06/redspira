package com.certuit.cercana.domain.dto;

import com.certuit.cercana.domain.entity.ActividadTicket;

import java.io.Serializable;

public class CommentDTO implements Serializable {
    private String message;
    private String status;
    private ActividadTicket data;


    public CommentDTO(){}

    public CommentDTO(ActividadTicket tickets, String mensaje, String status) {
        this.data = tickets;
        this.message = mensaje;
        this.status = status;
    }

    public ActividadTicket getData() {
        return data;
    }

    public void setData(ActividadTicket data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
