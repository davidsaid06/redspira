package com.certuit.cercana.domain.dto;

import com.certuit.cercana.domain.entity.Ticket;

import java.util.ArrayList;
import java.util.List;

public class TicketDTO {

    private String status;
    private String message = "";
    private List<Ticket> data = new ArrayList();

    public TicketDTO(){

    }

    public TicketDTO(String status, String message, List<Ticket> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Ticket> getData() {
        return data;
    }

    public void setData(List<Ticket> data) {
        this.data = data;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
