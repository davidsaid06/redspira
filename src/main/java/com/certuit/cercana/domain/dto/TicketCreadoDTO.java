package com.certuit.cercana.domain.dto;

import java.io.Serializable;

public class TicketCreadoDTO implements Serializable {

    private String status;
    private String message;
    private TicketCreadoNumero data;

    public TicketCreadoDTO(){}

    public TicketCreadoDTO(String status, String message, TicketCreadoNumero data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TicketCreadoNumero getData() {
        return data;
    }

    public void setData(TicketCreadoNumero data) {
        this.data = data;
    }
}
