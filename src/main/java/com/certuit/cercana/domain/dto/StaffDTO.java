package com.certuit.cercana.domain.dto;

import com.certuit.cercana.domain.entity.Ticket;

import java.util.ArrayList;
import java.util.List;

public class StaffDTO {

    private int id;
    private String name;
    private String email;
    private String profilePic;
    private String level;
   // private List<Departamento>  departments;
    private List<Ticket> tickets = new ArrayList();

    public StaffDTO(){

    }

    public StaffDTO(int id, String name, String email, String profilePic, int level, List<Ticket> tickets) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.profilePic = profilePic;
        this.level = level+"";
     //   this.departments = departments;
        this.tickets = tickets;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level+"";
    }


    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
