package com.certuit.cercana.domain.dto;

public class ValidateData {
    Integer type;

    public ValidateData(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
