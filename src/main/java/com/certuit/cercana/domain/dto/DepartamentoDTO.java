package com.certuit.cercana.domain.dto;

import com.certuit.cercana.domain.entity.Departamento;

import java.util.ArrayList;
import java.util.List;

public class DepartamentoDTO {

    private String status;
    private String message = "";
    private List<Departamento> data = new ArrayList();

    public DepartamentoDTO(){

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Departamento> getData() {
        return data;
    }

    public void setData(List<Departamento> data) {
        this.data = data;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
