package com.certuit.cercana.domain.dto;

import com.certuit.cercana.domain.entity.Articulo;

import java.io.Serializable;
import java.util.List;

public class TopicDTO implements Serializable {

    private String id;
    private String name;
    private List<Articulo> articles;

    public TopicDTO(String id, String name, List<Articulo> data) {
        this.id = id;
        this.name = name;
        this.articles = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Articulo> getArticles() {
        return articles;
    }

    public void setArticles(List<Articulo> data) {
        this.articles = data;
    }
}
