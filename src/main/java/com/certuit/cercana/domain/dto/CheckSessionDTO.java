package com.certuit.cercana.domain.dto;

public class CheckSessionDTO {

    private String status;
    private String message = "";
    private Boolean data;

    public CheckSessionDTO(){

    }

    public CheckSessionDTO(String status, String message, Boolean data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message){ this.message = message; }

    public String getMessage() { return message; }

    public Boolean getData() {
        return data;
    }

    public void setData(Boolean data) {
        this.data = data;
    }
}
