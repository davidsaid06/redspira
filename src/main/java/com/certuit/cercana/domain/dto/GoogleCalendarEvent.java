package com.certuit.cercana.domain.dto;

import com.google.api.client.util.DateTime;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter @Setter
public class GoogleCalendarEvent {

    private String calendarId;
    private int maxAttendees;
    private boolean sendNotifications;
    private String sendUpdates;
    private boolean supportsAttachments;
    private DateTime startDateTime;
    private DateTime endDateTime;
    private List<String> attendees;
    private String colorId;
    private String summary;
    private String description;
    private String location;
    private String timeZone;
    private String visibility;


    public GoogleCalendarEvent() {
    }

    public GoogleCalendarEvent(String calendarId, String summary, String description, String timeZone, DateTime startDateTime, DateTime endDateTime) {
        this.calendarId = calendarId;
        this.summary = summary;
        this.description = description;
        this.timeZone = timeZone;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

    public GoogleCalendarEvent(String calendarId, List<String> attendees, String summary, String description, String timeZone, DateTime startDateTime, DateTime endDateTime) {
        this.calendarId = calendarId;
        this.attendees = attendees;
        this.summary = summary;
        this.description = description;
        this.timeZone = timeZone;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

}
