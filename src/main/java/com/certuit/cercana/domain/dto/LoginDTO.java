package com.certuit.cercana.domain.dto;

import java.io.Serializable;

public class LoginDTO implements Serializable {

    private String status;
    private String message;
    private LoginData data;

    public LoginDTO(){}

    public LoginDTO(String status, String message, LoginData data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }
}
