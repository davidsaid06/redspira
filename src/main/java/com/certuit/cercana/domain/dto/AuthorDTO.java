package com.certuit.cercana.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AuthorDTO implements Serializable {
    @JsonProperty("id_int")
    private int id;
    @JsonProperty("id")
    private String id_string;
    private String name;
    private String email;
    private String profilePic;
    private Boolean staff;

    public AuthorDTO(){}

    public AuthorDTO(int id, String name, String email, String profilePic, Boolean staff) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.profilePic = profilePic;
        this.staff = staff;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Boolean getStaff() {
        return staff;
    }

    public void setStaff(Boolean staff) {
        this.staff = staff;
    }

    public void setId_string(String id_string){
        this.id_string=id_string;
    }
}
