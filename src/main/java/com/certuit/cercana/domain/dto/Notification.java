package com.certuit.cercana.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
@Getter @Setter
public class Notification{

	@Getter @Setter
	private String sound;
	@Getter @Setter
	private String title;
	@Getter @Setter
	private String body;
	@Getter @Setter
	@JsonProperty("click_action")
	private String clickAction;
	@Getter @Setter
	private int id;
	@Getter @Setter
	private int referencia;
	@Getter @Setter
	private String nombre;

	public Notification() {
	}

	public Notification(String sound, String title, String body) {
		this.sound = sound;
		this.title = title;
		this.body = body;
	}

	public Notification(String sound, String title, String body, String clickAction) {
		this.sound = sound;
		this.title = title;
		this.body = body;
		this.clickAction = clickAction;

	}

//	public Notification(String sound, String title, String body, String click_action) {
//		this.sound = sound;
//		this.title = title;
//		this.body = body;
//		this.click_action = click_action;
//
//	}

	public Notification(String sound, String title, String body, String clickAction, int id) {
		this.sound = sound;
		this.title = title;
		this.body = body;
		this.clickAction = clickAction;
		this.id = id;
	}

	public Notification(String sound, String title, String body, String clickAction, String nombre) {
		this.sound = sound;
		this.title = title;
		this.body = body;
		this.clickAction = clickAction;
		this.nombre = nombre;
	}

}
