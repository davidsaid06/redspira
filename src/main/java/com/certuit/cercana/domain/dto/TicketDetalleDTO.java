package com.certuit.cercana.domain.dto;

import com.certuit.cercana.domain.entity.Ticket;

public class TicketDetalleDTO {

    private String status;
    private String message = "";
    private Ticket data;

    public TicketDetalleDTO(){

    }

    public TicketDetalleDTO(String status, String message, Ticket data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Ticket getData() {
        return data;
    }

    public void setData(Ticket data) {
        this.data = data;
    }
}
