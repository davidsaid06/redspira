package com.certuit.cercana.domain.dto;

public class TicketAssignDTO {

    private String status;
    private String message = "";
    private String data;

    public TicketAssignDTO(){

    }

    public TicketAssignDTO(String status, String message, String data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
