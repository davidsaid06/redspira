package com.certuit.cercana.domain.dto;

import com.certuit.cercana.domain.entity.Ticket;

import java.io.Serializable;
import java.util.List;

public class UserDataDTO implements Serializable {
    private String name;
    private String email;
    private Boolean verified;
    private List<Ticket> tickets;


    public UserDataDTO(){}

    public UserDataDTO(String name, String email, List<Ticket> tickets) {
        this.tickets = tickets;
        this.name = name;
        this.email = email;
        this.verified = true;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
