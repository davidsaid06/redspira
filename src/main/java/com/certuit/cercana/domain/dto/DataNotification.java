package com.certuit.cercana.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class DataNotification {

	@Getter @Setter
	private int id;
	@Getter @Setter
	private String titulo;
	@Getter @Setter
	private String mensaje;
	@Getter @Setter
	private String nombre;
	@Getter @Setter
	private String descripcion;
	@Getter @Setter
	private String direccion;
	@Getter @Setter
	private String contacto;
	@Getter @Setter
	@JsonProperty("url_imagen")
	private String urlImagen;
	@Getter @Setter
	@JsonProperty("click_action")
	private String clickAction;



	public DataNotification() {
	}



}
