package com.certuit.cercana.domain.dto;

import java.io.Serializable;
import java.util.List;

public class AvisosDTO implements Serializable {

    private String status;
    private String message;
    private List<TopicDTO> data;

    public AvisosDTO(String status, String message, List<TopicDTO> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TopicDTO> getData() {
        return data;
    }

    public void setData(List<TopicDTO> data) {
        this.data = data;
    }
}
