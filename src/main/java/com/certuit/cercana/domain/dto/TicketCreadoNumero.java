package com.certuit.cercana.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TicketCreadoNumero implements Serializable {

    @JsonProperty("ticketNumber")
    private Integer ticketNumber;

    public TicketCreadoNumero(Integer ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Integer getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(Integer ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
}
