package com.certuit.cercana.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */
public class Push {

	@Getter @Setter
	private String to;

	@Getter @Setter
	private String priority;

	@Getter @Setter
	private Notification notification;

	@Getter @Setter
	@JsonProperty(value = "registration_ids")
	private List<String> registrationIds;

	@Getter @Setter
	private DataNotification data;

	/**
	 * 
	 * @param priority
	 * @param notification
	 * @param registrationds
	 */
	public Push(String priority, Notification notification, List<String> registrationds) {
		this.priority = priority;
		this.notification = notification;
		this.registrationIds = registrationds;
//        this.data = notification;

	}

	/**
	 *
	 * @param to
	 * @param priority
	 * @param notification
	 */
	public Push(String to, String priority, Notification notification) {
		this.to = to;
		this.priority = priority;
		this.notification = notification;

	}

	/**
	 *
	 * @param to
	 * @param priority
	 * @param dataNotification
	 */
	public Push(String to, String priority, DataNotification dataNotification) {
		this.to = to;
		this.priority = priority;
		this.data = dataNotification;

	}

	/**
	 * 
	 * @param to
	 * @param priority
	 * @param notification
	 */
	public Push(String to, String priority, Notification notification, DataNotification dataNotification) {
		this.to = to;
		this.priority = priority;
		this.notification = notification;
		this.data = dataNotification;

	}

	/**
	 * 
	 */
	public Push() {

	}


}
