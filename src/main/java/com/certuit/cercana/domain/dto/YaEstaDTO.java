package com.certuit.cercana.domain.dto;

import java.io.Serializable;

public class YaEstaDTO implements Serializable {
    private String message;
    private String status;
    private UserDataDTO data;


    public  YaEstaDTO(){}

    public YaEstaDTO(UserDataDTO tickets, String mensaje, String status) {
        this.data = tickets;
        this.message = mensaje;
        this.status = status;
    }

    public UserDataDTO getData() {
        return data;
    }

    public void setData(UserDataDTO data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
