package com.certuit.cercana.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginData {

    @JsonProperty("userId")
    private String userId;
    @JsonProperty("userEmail")
    private String userEmail;
    private String staff;
    private String token;
    @JsonProperty("rememberToken")
    private String rememberToken;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("deviceType")
    private String deviceType;

    public LoginData(){}

    public LoginData(String userId, String userEmail, String staff, String token, String rememberToken, String deviceId, String deviceType) {
        this.userId = userId;
        this.userEmail = userEmail;
        this.staff = staff;
        this.token = token;
        this.rememberToken = rememberToken;
        this.deviceId = deviceId;
        this.deviceType = deviceType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getStaff() {
        return staff;
    }

    public void setStaff(String staff) {
        this.staff = staff;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }
}
