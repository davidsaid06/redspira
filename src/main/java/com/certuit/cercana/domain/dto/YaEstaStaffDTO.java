package com.certuit.cercana.domain.dto;

import java.io.Serializable;

public class YaEstaStaffDTO implements Serializable {
    private String message;
    private String status;
    private StaffDTO data;


    public YaEstaStaffDTO(){}

    public YaEstaStaffDTO(String message, String status, StaffDTO data) {
        this.message = message;
        this.status = status;
        this.data = data;
    }

    public StaffDTO getData() {
        return data;
    }

    public void setData(StaffDTO data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
