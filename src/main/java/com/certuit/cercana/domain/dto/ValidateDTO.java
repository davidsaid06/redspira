package com.certuit.cercana.domain.dto;

public class ValidateDTO {

    private String status;
    private String message;
    private ValidateData data;

    public ValidateDTO(){}

    public ValidateDTO(String status, String message, ValidateData data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public ValidateData getData() {
        return data;
    }

    public void setData(ValidateData data) {
        this.data = data;
    }
}
