package com.certuit.cercana.domain.base;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@MappedSuperclass
@Getter
@Setter
public class EntityBase implements Comparable, java.io.Serializable {

    @Id
//    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @Transient
    private boolean selected = false;

    @Embedded
    private Audit audit;

    public EntityBase() {

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EntityBase other = (EntityBase) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public int compareTo(Object obj) {
        if (obj.hashCode() > hashCode()) return 1;
        else if (obj.hashCode() < hashCode()) return -1;
        else return 0;
    }

}
