package com.certuit.cercana.domain.base;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Date;

@Getter
@Setter
@Embeddable
public class Audit {

    @Column(name = "created_on", columnDefinition = "TIMESTAMP")
    private Date createdOn;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_on", columnDefinition = "TIMESTAMP")
    private Date updatedOn;

    @Column(name = "updated_by")
    private String updatedBy;
}
