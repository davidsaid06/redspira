package com.certuit.cercana.domain.base;

public interface Auditable {
    Audit getAudit();

    void setAudit(Audit audit);
}
