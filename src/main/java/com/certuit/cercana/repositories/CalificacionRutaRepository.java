package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.CalificacionRuta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CalificacionRutaRepository extends JpaRepository<CalificacionRuta, Integer> {

    CalificacionRuta deleteById(int id);
    CalificacionRuta findAllById(int id);
}
