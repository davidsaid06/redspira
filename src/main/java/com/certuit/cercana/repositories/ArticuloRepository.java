package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.Articulo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticuloRepository extends JpaRepository<Articulo, Integer> {

    Articulo findAllById(Integer id);
}
