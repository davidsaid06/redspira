package com.certuit.cercana.repositories;


import com.certuit.cercana.domain.entity.SolicitudContrasena;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SolicitudContrasenaRepository extends JpaRepository<SolicitudContrasena, Integer> {


    SolicitudContrasena findFirstByLlave(String llave);


}