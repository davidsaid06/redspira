package com.certuit.cercana.repositories;


import com.certuit.cercana.domain.entity.Notificacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface NotificacionRepository extends JpaRepository<Notificacion, Integer> {
//    Notificacion findByNombre(String role);

     List<Notificacion> findByUsuario_IdOrderByFechaCreacionDesc(int id);

     int countAllByUsuario_Id(int id);
}