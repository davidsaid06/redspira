package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.ParametroSistema;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ParametroSistemaRepository extends JpaRepository<ParametroSistema, Integer> {


    List<ParametroSistema> findByPropiedad(String propiedad);

    ParametroSistema findFirstByPropiedad(String propiedad);


}