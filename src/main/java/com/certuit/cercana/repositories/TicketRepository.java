package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {

   Ticket findAllById(Integer id);

   List<Ticket> findAllByUnreadEquals(boolean stat);

   List<Ticket> findAllByStaffId(Integer id);

   List<Ticket> findAllByStaffIdAndClosed(Integer id, Boolean bol);

   List<Ticket> findAllByEstatus(String estatus);

   Ticket findByTicketNumber(String tNumber);

   Integer countAllByDepartmentIdEqualsAndStaffIsNull(Integer department_id);

   // Global
   List<Ticket> findAllByTicketNumberContainingOrContentContainingOrTitleContainingOrUsuario_NameContaining(String query1, String query2, String query3, String query4);

   // Cerrados y Sin Cerrar (Todos)
   List<Ticket> findAllByClosedEqualsOrderByDateAsc(Boolean cerrado);

   // Nuevos (Todos)
   List<Ticket> findAllByClosedEqualsAndStaffIsNullAndDepartmentIsNotNullOrderByDateAsc(Boolean cerrado);

   // Nuevos (Por departamento)
   List<Ticket> findAllByClosedEqualsAndStaffIsNullAndDepartmentIdEqualsOrderByDateAsc(Boolean cerrado, Integer i);

   // Cerrados y Sin Cerrar (Por Usuario)
   List<Ticket> findAllByClosedEqualsAndStaffIdEqualsOrderByTicketNumberDesc(Boolean cerrado, Integer staff);

   // Cerrados y Sin Cerrar (Por Departamento)
   List<Ticket> findAllByClosedEqualsAndDepartmentIdEqualsOrderByDateAsc(Boolean cerrado, Integer depa);

   // Cerrados y Sin Cerrar (Por Usuario y Departamento)
   List<Ticket> findAllByClosedEqualsAndDepartmentIdEqualsAndStaffIdEqualsOrderByTicketNumberDesc(Boolean cerrado, Integer depa, Integer staff);

   // Respuesta Ciudadano
   List<Ticket> findAllByUnreadStaffEquals(boolean stat);

   // Respuesta ciudadano y departamento
   List<Ticket> findAllByUnreadStaffEqualsAndDepartmentIdEquals(boolean stat, Integer depa);

   // Tickets del usuario por status
   List<Ticket> findAllByUsuarioIdAndClosed(int authorId, boolean close );

   // Tickets del usuario
   List<Ticket> findAllByUsuarioId(int authorId);

   // Max ticket Number
   @Query("SELECT coalesce(max(t.id), 0) FROM Ticket t")
   Integer getMaxTicketNumber();



}
