package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.Departamento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartamentoRepository extends JpaRepository<Departamento, Integer> {

    Departamento findAllById(Integer id);
}
