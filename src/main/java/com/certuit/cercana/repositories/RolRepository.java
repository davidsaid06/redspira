package com.certuit.cercana.repositories;


import com.certuit.cercana.domain.entity.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface RolRepository extends JpaRepository<Rol, Integer> {
    Rol findByNombre(String role);

    List<Rol> findAllByIdNot(int id);
}