package com.certuit.cercana.repositories;


import com.certuit.cercana.domain.entity.RespuestaRapida;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RespuestaRapidaRepository extends JpaRepository<RespuestaRapida, Integer> {


     List<RespuestaRapida> findByVigente(boolean vigente);

     List<RespuestaRapida> findByVigenteOrderByNombreAsc(boolean vigente);

     List<RespuestaRapida> findAllByNombreContainingAndVigenteOrderByNombreAsc(String nombre, boolean vigente);


}