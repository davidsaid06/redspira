package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.RutaObservaciones;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RutaObservacionesRepository extends JpaRepository<RutaObservaciones, Integer> {

    RutaObservaciones findAllById(int id);
    List<RutaObservaciones> findAllBySectorEquals(String str);

}
