package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.HorarioRuta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HorarioRutaRepository extends JpaRepository<HorarioRuta, Integer> {

    HorarioRuta findAllById(int id);

}
