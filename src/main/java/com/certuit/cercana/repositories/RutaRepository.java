package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.Ruta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RutaRepository extends JpaRepository<Ruta, Integer> {

    Ruta findAllById(int id);
    List<Ruta> findAllBySectorEquals(String str);
    List<Ruta> findAllByHorarioTurnoEquals(Integer i);
    List<Ruta> findAllByHorarioTurnoAndSectorEquals(Integer i, String str);

}
