package com.certuit.cercana.repositories;


import com.certuit.cercana.domain.entity.Mensaje;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MensajeRepository extends JpaRepository<Mensaje, Integer> {


    Mensaje findFirstByTipoNotificacion(int tipoNotificacion);

}