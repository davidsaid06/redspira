package com.certuit.cercana.repositories;


import com.certuit.cercana.domain.entity.ActividadTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ActividadTicketRepository extends JpaRepository<ActividadTicket, Integer> {

    List<ActividadTicket> findAllByTicketId(int id);

    List<ActividadTicket> findAllByTicketIdAndTipoEqualsOrderByFechaActividadAsc(int id, int tipo);

    int countAllByTicketIdAndUsuarioIdAndTipo(int id, int usuario, int tipo);

    ActividadTicket findFirstByTicketId(int id);

}