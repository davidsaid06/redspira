package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.Direccion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DireccionRepository extends JpaRepository<Direccion, Integer> {

   Direccion findAllById(int id);
   List<Direccion> findAllByUserId(int user_id);
   //ArrayList<Direccion> findAllByUser_id(int userId);
}
