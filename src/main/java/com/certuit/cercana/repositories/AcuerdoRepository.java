package com.certuit.cercana.repositories;


import com.certuit.cercana.domain.entity.Acuerdo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AcuerdoRepository extends JpaRepository<Acuerdo, Integer> {


    Acuerdo findByTipo(String tipo);

}