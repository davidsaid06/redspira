package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.CatalogoParametro;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CatalogoParametroRepository extends JpaRepository<CatalogoParametro, Integer> {

    List<CatalogoParametro> findAll();
}
