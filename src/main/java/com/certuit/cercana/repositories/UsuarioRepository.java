package com.certuit.cercana.repositories;


import com.certuit.cercana.domain.entity.Rol;
import com.certuit.cercana.domain.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    Usuario findFirstByEmail(String username);

    List<Usuario> findAllByRolesIn(List<Rol> roles);

    Usuario findFirstByFacebookId(String facebookId);

    Usuario findAllById(Integer id);

    List<Usuario> findAllByRolesInAndDepartmentsIdEquals(List<Rol> roles, Integer depa);

    List<Usuario> findAllByRolesInAndDepartmentsIdEqualsAndLevelGreaterThanEqual(List<Rol> roles, Integer depa, int level);

}