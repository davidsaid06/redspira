package com.certuit.cercana.repositories;

import com.certuit.cercana.domain.entity.Camion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CamionRepository extends JpaRepository<Camion, Integer> {

    List<Camion> findAllByCamionApoyoEquals(Boolean bol);

}
