package com.certuit.cercana.service.Redspira;

import com.certuit.cercana.domain.entity.CatalogoParametro;
import com.certuit.cercana.repositories.CatalogoParametroRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogoParametroService extends ServiceBase<CatalogoParametro, Integer> {

    @Autowired
    private CatalogoParametroRepository parametroRepo;

    public CatalogoParametroService(CrudRepository<CatalogoParametro, Integer> repo) {
        super(repo);
    }

    public List<CatalogoParametro> findAll()
    {
        System.out.println("Entre por aca 2");
        return parametroRepo.findAll();
    }


}
