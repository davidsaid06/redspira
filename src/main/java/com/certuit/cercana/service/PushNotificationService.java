package com.certuit.cercana.service;

import com.certuit.cercana.domain.dto.Push;
import com.certuit.cercana.filter.HeaderRequestInterceptor;
import com.certuit.cercana.util.FirebaseResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 *
 *
 * <p>
 * <b> Push Notification Service </b>
 * </p>
 *
 * <p>
 * Service send message to FCM
 * </p>
 *
 *
 * @author <a href="https://github.com/FranckAJ">Franck Aragão</a>
 *
 */

@Service
public class PushNotificationService {


	private static final String FCM_KEY = "AAAA9uYB6B0:APA91bGJEAwZMWAArP5J4_V2mvxAXGW0ahOsfUcbYift7LCWy3xFQl8l46BiYkjAVT1Iepjrj9qh7WulGiTDdmE-pz46FNjzYGwIAWFvIme1mJWIHc9IgAP0beuuFQlzROHnvMqJLD2P";
	//key antigua
	// AAAAWU5rWLI:APA91bGQE82buBcuTEQzeiZSHpxfJJBNgGNOu_MqN0y7hqcuydnyfqkg77siHxolgY-ABnXCk-inS7hQ1reDCPNDfCtHtxm1wbMRjCwUgbNPXkuXdFNQ3aETqKW6-Btx5BIvf6ZGgJRqbWl1fBpO8T5OKOixRVGPAw

	private static final String FCM_API = "https://fcm.googleapis.com/fcm/send";

	/**
	 *
	 * @param push
	 * @return
	 */

	public FirebaseResponse sendNotification(Push push) {

		HttpEntity<Push> request = new HttpEntity<>(push);

		CompletableFuture<FirebaseResponse> pushNotification = this.send(request);
		CompletableFuture.allOf(pushNotification).join();

		FirebaseResponse firebaseResponse = null;

		try {
			firebaseResponse = pushNotification.get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
		return firebaseResponse;
	}

	/**
	 * send push notification to API FCM
	 *
	 * Using CompletableFuture with @Async to provide Asynchronous call.
	 *
	 *
	 * @param entity
	 * @return
	 */
	@Async
	public CompletableFuture<FirebaseResponse> send(HttpEntity<Push> entity) {

		RestTemplate restTemplate = new RestTemplate();

		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
		interceptors.add(new HeaderRequestInterceptor("Authorization", "key=" + FCM_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
		restTemplate.setInterceptors(interceptors);

		FirebaseResponse firebaseResponse = restTemplate.postForObject(FCM_API, entity, FirebaseResponse.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}
}
