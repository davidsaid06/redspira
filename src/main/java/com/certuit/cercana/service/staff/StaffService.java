package com.certuit.cercana.service.staff;

import com.certuit.cercana.domain.entity.Rol;
import com.certuit.cercana.domain.entity.Usuario;
import com.certuit.cercana.repositories.RolRepository;
import com.certuit.cercana.repositories.UsuarioRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StaffService extends ServiceBase<Usuario, Integer> {

    @Autowired
    private UsuarioRepository staffRepo;

    @Autowired
    private RolRepository rolRepository;

    public StaffService(CrudRepository<Usuario, Integer> repo) {
        super(repo);
    }

    //TODO Find by Role
    public List<Usuario> findAll() {
        List<Rol> roles = rolRepository.findAllByIdNot(com.certuit.cercana.domain.enums.Rol.USER.getId());
        return staffRepo.findAllByRolesIn(roles);
    }

    public Usuario findById(Integer id) {
        return staffRepo.findOne(id);
    }

    public List<Usuario> getByDepartamento(Integer id) {
        List<Rol> roles = rolRepository.findAllByIdNot(com.certuit.cercana.domain.enums.Rol.USER.getId());
        return staffRepo.findAllByRolesInAndDepartmentsIdEquals(roles, id);
}

    public List<Usuario> getByDepartamentoAndNivel(Integer id, int level) {
        List<Rol> roles = rolRepository.findAllByIdNot(com.certuit.cercana.domain.enums.Rol.USER.getId());
        return staffRepo.findAllByRolesInAndDepartmentsIdEqualsAndLevelGreaterThanEqual(roles, id, level);
    }

    public Usuario getStaffByEmail(String email) {
        return staffRepo.findFirstByEmail(email);
    }

}
