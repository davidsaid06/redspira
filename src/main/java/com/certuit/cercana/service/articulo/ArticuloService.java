package com.certuit.cercana.service.articulo;

import com.certuit.cercana.domain.entity.Articulo;
import com.certuit.cercana.domain.entity.Topic;
import com.certuit.cercana.repositories.ArticuloRepository;
import com.certuit.cercana.repositories.TopicRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;

@Service
public class ArticuloService extends ServiceBase<Articulo, Integer> {

    public ArticuloService(CrudRepository<Articulo, Integer> repo) {
        super(repo);
    }

    @Autowired
    private ArticuloRepository articuloRepo;

    @Autowired
    private TopicRepository topicRepository;

    public List<Articulo> findAll() {
        return articuloRepo.findAll();
    }

    public Articulo getArticuloID(Integer id) {
        return articuloRepo.findOne(id);
    }

    public Articulo postArticulo(String title, String content, Integer position,
                                 Integer topic_id, String nombre, String email) {

        Topic topic = this.topicRepository.findOne(topic_id);
        Articulo nuevo = new Articulo(title, content, new Date(), position, topic, nombre, email);
        if (nuevo != null) {
            return articuloRepo.save(nuevo);
        }
        throw new EntityNotFoundException("Error"); //TODO poner un error real
    }

    public Articulo putArticulo(Integer id, String title, String content,
                                int position, int topic_id, String nombre, String email) {
        Articulo articuloModificado = articuloRepo.findAllById(id);
        Topic topic = this.topicRepository.findOne(topic_id);
        articuloModificado.modificarArticulo(title, content, new Date(), position, topic, nombre, email);
        return articuloRepo.save(articuloModificado);
    }

    public Articulo deleteArticulo(int idArticulo) {
        Articulo articulo = articuloRepo.findAllById(idArticulo);
        if (articulo != null) {
            articuloRepo.delete(articulo);
            return articulo;
        }
        throw new EntityNotFoundException("No se encontro la con el id-" + idArticulo);
    }

}
