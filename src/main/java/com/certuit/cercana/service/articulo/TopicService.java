package com.certuit.cercana.service.articulo;

import com.certuit.cercana.domain.entity.Topic;
import com.certuit.cercana.repositories.TopicRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TopicService extends ServiceBase<Topic, Integer> {

    public TopicService(CrudRepository<Topic, Integer> repo) {
        super(repo);
    }

    @Autowired
    private TopicRepository topicRepository;

}
