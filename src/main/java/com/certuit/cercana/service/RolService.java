package com.certuit.cercana.service;

import com.certuit.cercana.domain.entity.Rol;
import com.certuit.cercana.repositories.RolRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Certuit
 */
@Service
public class RolService extends ServiceBase<Rol, Integer> {

    @Autowired
    private RolRepository repo;

    public RolService(CrudRepository<Rol, Integer> repo) {
        super(repo);
    }

    /**
     * @param entity
     * @return
     */
    @Transactional
    public Rol save(Rol entity) {
        return this.repo.save(entity);
    }

    public List<Rol> findAll() {
        return this.repo.findAll();
    }

    public Rol findOne(Integer id) {
        return this.repo.findOne(id);
    }

}
