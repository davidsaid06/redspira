package com.certuit.cercana.service.recoleccion;

import com.certuit.cercana.domain.entity.Camion;
import com.certuit.cercana.domain.entity.Direccion;
import com.certuit.cercana.repositories.CalificacionRutaRepository;
import com.certuit.cercana.repositories.CamionRepository;
import com.certuit.cercana.repositories.DireccionRepository;
import com.certuit.cercana.repositories.RutaRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RecoleccionService extends ServiceBase<Direccion, Integer> {

    @Autowired
    private CalificacionRutaRepository calificacionRepo;
    @Autowired
    private DireccionRepository direcccionRepo;
    @Autowired
    private CamionRepository camionRepo;
    @Autowired
    private RutaRepository rutaRepo;

    public RecoleccionService(CrudRepository<Direccion, Integer> repo) {
        super(repo);
    }


    public Direccion addAddress(String label, String address, Double lat, Double lng, Boolean noti, int zone_id, int userID) {
        return direcccionRepo.save(new Direccion(lat, lng, label, noti, zone_id, userID));
    }

    public List<Camion> getUnidadesDeApoyo() {
        return camionRepo.findAllByCamionApoyoEquals(true);
    }


}
