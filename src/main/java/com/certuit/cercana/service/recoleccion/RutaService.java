package com.certuit.cercana.service.recoleccion;
import com.certuit.cercana.domain.entity.Ruta;
import com.certuit.cercana.repositories.RutaRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

@Service
public class RutaService extends ServiceBase<Ruta, Integer> {

    @Autowired
    private RutaRepository rutaRepo;

    public RutaService(CrudRepository<Ruta, Integer> repo) {
        super(repo);
    }

    public List<Ruta> findAll() {
        return rutaRepo.findAll();
    }

    public Ruta findById(Integer id) {
        return rutaRepo.findOne(id);
    }

    public Ruta agregarRuta(String address, String label, int radius, String type, Polygon geom) {
        Ruta nuevo = new Ruta(address,label,radius,type,geom);
        return rutaRepo.save(nuevo);
    }

    public Ruta modificarRuta(Integer id, String address, String label, int radius, String type, Polygon geom) {
        Ruta rutaModificada = rutaRepo.findAllById(id);
        rutaModificada.setNuevosValores(address,label,radius,type,geom);
        return rutaRepo.save(rutaModificada);
    }

    public Ruta agregarObservaciones(Integer id, String observaciones, Integer sustituto, Timestamp date) {
        if(date == null){
            date = this.fechaEnXNumeroDeDias(1);
        }
        Ruta rutaModificada = rutaRepo.findAllById(id);
        rutaModificada.agregarObservaciones(observaciones,sustituto,date);
        return rutaRepo.save(rutaModificada);
    }

    public Ruta deleteByID(Integer RutaID) {
        Ruta ruta = rutaRepo.findAllById(RutaID);
        if (ruta != null) {
            rutaRepo.delete(ruta);
            return ruta;
        }
    return null;
    }

    public Ruta changeStatus(Integer id, Integer status) {
        Ruta rutaModificada = rutaRepo.findAllById(id);
        rutaModificada.changeStatus(status);
        return rutaRepo.save(rutaModificada);
    }

    public Ruta clearObservaciones(Integer id) {
        Ruta rutaModificada = rutaRepo.findAllById(id);
        if(rutaModificada.getFechaRestauracion() != null){
            if(rutaModificada.getFechaRestauracion().getTime() <= fechaEnXNumeroDeDias(0).getTime()){
            rutaModificada.agregarObservaciones("",0,null);
            }
        }
        return rutaRepo.save(rutaModificada);
    }

    public Timestamp fechaEnXNumeroDeDias(Integer numeroDeDias){
        Calendar cal = Calendar.getInstance();
        cal.setTime(Calendar.getInstance().getTime());
        cal.add(Calendar.DATE, numeroDeDias);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTime().getTime());
    }

    public List<Ruta> filtrar(String sector) {
        return rutaRepo.findAllBySectorEquals(sector);
    }

    public List<Ruta> filtrarPorTurno(String filtroActivo) {
        return rutaRepo.findAllByHorarioTurnoEquals(Integer.parseInt(filtroActivo));
    }

    public List<Ruta> filtrarAmbos(String filtroActivo, String sector) {
        return rutaRepo.findAllByHorarioTurnoAndSectorEquals(Integer.parseInt(filtroActivo),sector);
    }
}
