package com.certuit.cercana.service.recoleccion;

import com.certuit.cercana.domain.entity.Ruta;
import com.certuit.cercana.domain.entity.RutaObservaciones;
import com.certuit.cercana.repositories.RutaObservacionesRepository;
import com.certuit.cercana.repositories.RutaRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

@Service
public class RutaObservacionesService extends ServiceBase<RutaObservaciones, Integer> {

    @Autowired
    private RutaRepository rutaRepo;

    @Autowired
    private RutaObservacionesRepository rutaORepo;

    public RutaObservacionesService(CrudRepository<RutaObservaciones, Integer> repo) {
        super(repo);
    }

    public List<RutaObservaciones> findAll() {
        return rutaORepo.findAll();
    }

    public RutaObservaciones findById(Integer id) {
        return rutaORepo.findOne(id);
    }

    public RutaObservaciones agregarObservacionRuta(Ruta ruta) {
        RutaObservaciones nuevo = new RutaObservaciones(ruta.getNoRuta(),ruta.getCamion().getNumeroCamion(),
                ruta.getSustituto(),ruta.getObservaciones(),ruta.getStatus(),ruta.getHorario().getTurno(),ruta.getSector(),
                fechaEnXNumeroDeDias(0),ruta.getFechaRestauracion());
        return rutaORepo.save(nuevo);
    }

    public Timestamp fechaEnXNumeroDeDias(Integer numeroDeDias){
        Calendar cal = Calendar.getInstance();
        cal.setTime(Calendar.getInstance().getTime());
        cal.add(Calendar.DATE, numeroDeDias);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return new Timestamp(cal.getTime().getTime());
    }
    
}
