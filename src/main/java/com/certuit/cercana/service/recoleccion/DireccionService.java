package com.certuit.cercana.service.recoleccion;
import com.certuit.cercana.domain.entity.Direccion;
import com.certuit.cercana.repositories.DireccionRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DireccionService extends ServiceBase<Direccion, Integer> {

    @Autowired
    private DireccionRepository direccionRepo;

    public DireccionService(CrudRepository<Direccion, Integer> repo) {
        super(repo);
    }

    public List<Direccion> findAll() {
        return direccionRepo.findAll();
    }

    public Direccion findById(Integer id) {
        return direccionRepo.findOne(id);
    }

    public List<Direccion> findByUser(Integer id) {
        return direccionRepo.findAllByUserId(id);
    }

    public Direccion agregarDireccion(Double lat, Double lng, String label, Boolean noti,
                                      int zone_id, Boolean notification_e, int user_id) {
        Direccion nuevo = new Direccion(lat, lng,  label, noti, zone_id, notification_e, user_id);
        return direccionRepo.save(nuevo);
    }

    public Direccion modificarDireccion(Integer id, Double lat, Double lng, String label, Boolean noti,
                                        int zone_id, Boolean notification_e, int user_id) {
        Direccion direccionModificada = direccionRepo.findAllById(id);
        if (direccionModificada != null) {
            direccionModificada.setNuevosValores(lat, lng, label, noti, zone_id, notification_e, user_id);
            return direccionRepo.save(direccionModificada);
        }
        return null;
    }

    public Direccion deleteByID(Integer DireccionID) {
        Direccion userDireccion = direccionRepo.findAllById(DireccionID);
        if (userDireccion != null) {
            direccionRepo.delete(userDireccion);
            return userDireccion;
        }
        return null;
    }


}
