package com.certuit.cercana.service.recoleccion;

import com.certuit.cercana.domain.entity.HorarioRuta;
import com.certuit.cercana.repositories.HorarioRutaRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HorarioRutaService extends ServiceBase<HorarioRuta, Integer> {

    @Autowired
    private HorarioRutaRepository horarioRutaRepo;

    public HorarioRutaService(CrudRepository<HorarioRuta, Integer> repo) {
        super(repo);
    }

    public List<HorarioRuta> getHorarios() { return horarioRutaRepo.findAll(); }

    public HorarioRuta getHorarioID(Integer id) {
        return horarioRutaRepo.findOne(id);
    }

    public HorarioRuta postHorario(String tracker_id, int d1, int d2,
                                   int turno,String entrada,String salida) {
        HorarioRuta nuevo = new HorarioRuta(tracker_id,d1,d2,turno,entrada,salida);
        return horarioRutaRepo.save(nuevo);
    }

    public HorarioRuta putHorario(int id, String tracker_id, int d1, int d2,
                                  int turno,String entrada,String salida) {

        HorarioRuta horarioModificado = horarioRutaRepo.findAllById(id);
        if (horarioModificado != null) {
            horarioModificado.setNuevosValores(tracker_id,d1,d2,turno,entrada,salida);
            return horarioRutaRepo.save(horarioModificado);
        }
        return null;
    }


    public HorarioRuta deleteHorario(int idHorarioRuta) {
        HorarioRuta horaRuta = horarioRutaRepo.findAllById(idHorarioRuta);
        if (horaRuta != null){
            horarioRutaRepo.delete(horaRuta);
            return horaRuta;
        }
        return null;
    }




}
