package com.certuit.cercana.service.recoleccion;

import com.certuit.cercana.domain.entity.CalificacionRuta;
import com.certuit.cercana.repositories.CalificacionRutaRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;


@Service
public class CalificacionRutaService extends ServiceBase<CalificacionRuta, Integer> {

    @Autowired
    private CalificacionRutaRepository caliRutaRepo;


    public CalificacionRutaService(CrudRepository<CalificacionRuta, Integer> repo) {
        super(repo);
    }

    public List<CalificacionRuta> getCalificaciones() { return caliRutaRepo.findAll(); }

    public CalificacionRuta getCalificacionPorID(@PathVariable Integer id) {
        return caliRutaRepo.findOne(id);
    }

    public CalificacionRuta postCalificacion(String calificacion, int zone, int user) {
        Timestamp currentTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
        CalificacionRuta calificacionGuardada = new CalificacionRuta(calificacion,zone,user,currentTimestamp);
        return calificacionGuardada;
    }

    public CalificacionRuta putCalificacion(int id, String calificacion) {
        CalificacionRuta calificacionModificada = caliRutaRepo.findAllById(id);
        if(calificacionModificada != null){
        calificacionModificada.setCalificacion(calificacion);
        return calificacionModificada;}
        return null;
    }

    public CalificacionRuta deleteCalificacion(@PathVariable int id) {
       CalificacionRuta calificacionRuta = caliRutaRepo.findAllById(id);
       if(calificacionRuta != null){
       caliRutaRepo.delete(calificacionRuta);
       return calificacionRuta;}
       return null;
    }



}
