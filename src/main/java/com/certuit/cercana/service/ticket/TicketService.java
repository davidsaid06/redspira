package com.certuit.cercana.service.ticket;

import com.certuit.cercana.domain.dto.DataNotification;
import com.certuit.cercana.domain.dto.Notification;
import com.certuit.cercana.domain.dto.Push;
import com.certuit.cercana.domain.entity.*;
import com.certuit.cercana.domain.enums.ClickAction;
import com.certuit.cercana.repositories.DepartamentoRepository;
import com.certuit.cercana.repositories.TicketRepository;
import com.certuit.cercana.service.NotificacionService;
import com.certuit.cercana.service.PushNotificationService;
import com.certuit.cercana.service.UsuarioService;
import com.certuit.cercana.service.base.ServiceBase;
import com.certuit.cercana.service.staff.StaffService;
import com.certuit.cercana.util.FirebaseResponse;
import com.certuit.cercana.util.JsfUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class TicketService extends ServiceBase<Ticket, Integer> {
    private static final String PATH_IMG = "tickets/images/";

    @Autowired
    private TicketRepository ticketRepo;
    @Autowired
    private DepartamentoRepository departamentoRepository;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private StaffService staffService;
    @Autowired
    private ActividadTicketService actividadTicketService;
    @Autowired
    private NotificacionService notificacionService;
    @Autowired
    private PushNotificationService pushNotificationService;

    public TicketService(CrudRepository<Ticket, Integer> repo) {
        super(repo);
    }

    public List<Ticket> getTickets() {
        return ticketRepo.findAll();
    }

    public Ticket getTicketID(Integer id) {
        Ticket ticket = ticketRepo.findOne(id);
        if (ticket != null) {
            List<ActividadTicket> eventos = this.actividadTicketService.findAllByTicket(ticket.getId());
            ticket.setEvents(eventos);
        }
        return ticket;
    }

    public Ticket getTicketAsUserStaff(Integer id, Integer idStaff) {
        Ticket ticket = ticketRepo.findOne(id);
        if (ticket.getUsuario().getId() == idStaff) {
            readTicketStaff(id, false);
            return ticket;
        }
        return ticket;
    }


    public List<Ticket> getNewTickets(){

        return ticketRepo.findAllByClosedEqualsAndStaffIsNullAndDepartmentIsNotNullOrderByDateAsc(false);
    }

    public Ticket getTicketAsUser(Integer id, Integer idUser) {
        Ticket ticket = ticketRepo.findOne(id);
        if (ticket.getUsuario().getId() == idUser) {
            readTicketUser(id, false);
            return ticket;
        }
        return ticket;
    }

    public List<Ticket> getTicketsByReadStatus(Boolean bool) {
        return ticketRepo.findAllByUnreadEquals(bool);
    }

    public List<Ticket> getTicketsByStaffReadStatus(Boolean bool) {
        return ticketRepo.findAllByUnreadStaffEquals(bool);
    }

    public List<Ticket> getTicketsByStaffReadStatusAndDepartment(Boolean bool, Integer depa) {
        return ticketRepo.findAllByUnreadStaffEqualsAndDepartmentIdEquals(bool, depa);
    }

    public List<Ticket> getTicketsStaffMember(Integer id) {
        return ticketRepo.findAllByStaffId(id);
    }

    public List<Ticket> getTicketsByAuthor(int authorID) {
        List<Ticket> tickets = ticketRepo.findAllByUsuarioId(authorID);
        for (Ticket ticket : tickets) {
            if (ticket != null) {
                List<ActividadTicket> eventos = this.actividadTicketService.findAllByTicket(ticket.getId());
                ticket.setEvents(eventos);
            }
        }
        return tickets;
    }

    public List<Ticket> getTicketsByAuthorAndStatus(int authorID, boolean closed) {
        return ticketRepo.findAllByUsuarioIdAndClosed(authorID, closed);
    }


    public Ticket postTicket(String tnumber, boolean unread, String priority, boolean unread_staff,
                             String title, String content, String language, Date date,
                             boolean closed,
                             Integer departmen_id, Usuario author_id, Usuario owner_id, String location, String lat,
                             String lon, String location_description, Date closed_date, Integer calification,
                             String estatus, MultipartFile file) throws IOException {
        String urlImage = "";
        Departamento departamento = this.departamentoRepository.findOne(departmen_id);
        Ticket nuevo = new Ticket(tnumber, unread, priority, unread_staff, title, content, language, date, closed, departamento, author_id, owner_id, location, lat, lon, location_description, closed_date, calification, estatus);
        if (file != null) {
            String fileName = file.getOriginalFilename();
            // extrae la extension del archivo y lo almacena en la variable
            file.getOriginalFilename().substring((fileName.lastIndexOf(('.'), fileName.length())));
            urlImage = JsfUtil.saveFile(fileName, PATH_IMG, file.getInputStream());
            nuevo.setImage(urlImage);
        }
        return ticketRepo.save(nuevo);
    }

    public Ticket putTicket(Integer id, String tnumber, boolean unread, String priority, boolean unread_staff,
                            String title, String content, String language, String file, Date date,
                            boolean closed, String author_email, String author_name,
                            Integer departmen_id, Integer author_id, Integer owner_id, String location, String lat,
                            String lon, String location_description, Date closed_date, Integer calification,
                            String estatus) {
        Ticket ticketModificado = ticketRepo.findOne(id);
        Usuario staff = this.staffService.findById(owner_id);
        Usuario usuario = this.usuarioService.findOne(author_id);
        Departamento departamento = this.departamentoRepository.findOne(departmen_id);
        if (ticketModificado != null) {
            ticketModificado.setNuevosValores(tnumber, unread, priority, unread_staff, title, content, language, date,
                    closed, departamento, usuario, staff, location,
                    lat, lon, location_description, closed_date, calification, estatus);
            return ticketRepo.save(ticketModificado);
        }
        return null;
    }

    public Ticket commentTicket(Integer id, String comment, Data data) {
        Ticket ticketModificado = ticketRepo.findOne(id);
        if (ticketModificado != null) {
            readTicketStaff(ticketModificado.getId(), true);
            readTicketUser(ticketModificado.getId(), true);
            ticketModificado.commentTicket(comment, data);
            return ticketRepo.save(ticketModificado);
        }
        return null;
    }

    public Ticket deleteTicket(int idTicket) {
        Ticket ticket = ticketRepo.findOne(idTicket);
        if (ticket != null) {
            ticketRepo.delete(ticket);
            return ticket;
        }
        return null;
    }

    public Ticket ticketChangeDepartment(Integer id, Integer newDepa) {
        Ticket ticketModificada = ticketRepo.findOne(id);
        Departamento departamento = this.departamentoRepository.findOne(newDepa);
        if (ticketModificada != null) {
            ticketModificada.changeDepartment(departamento);
            readTicketStaff(ticketModificada.getId(), true);
            readTicketUser(ticketModificada.getId(), true);
            return ticketModificada;
        }
        return null;
    }

    public Ticket ticketClose(Integer id) {
        Ticket ticketModificada = ticketRepo.findOne(id);
        if (ticketModificada != null) {
            if (ticketModificada.getClosedStatus()) {
                return null;
            }
            ticketModificada.setClosed(true, new Date());

            readTicketStaff(ticketModificada.getId(), true);
            readTicketUser(ticketModificada.getId(), true);

            return ticketRepo.save(ticketModificada);
        }
        return null;
    }

    public Ticket ticketOpenClose(Integer id, Boolean close) {
        Ticket ticketModificada = ticketRepo.findOne(id);
        if (ticketModificada != null) {
            ticketModificada.setClosed(close, new Date());

            readTicketStaff(ticketModificada.getId(), true);
            readTicketUser(ticketModificada.getId(), true);

            return ticketRepo.save(ticketModificada);
        }
        return null;
    }

    public Ticket ticketQualify(Integer id, Integer calif) {
        Ticket ticketModificada = ticketRepo.findOne(id);

        if (ticketModificada != null) {
            ticketModificada.setCalification(calif);
            return ticketRepo.save(ticketModificada);
        }
        return null;
    }


    public List<Ticket> getUnassignedTickets() {
        List<Ticket> newTickets = ticketRepo.findAllByEstatus("Por Asignar");
        return newTickets;
    }

    public Ticket assignTicket(Integer staffId, Integer ticket) {
        Ticket ticketModificada = ticketRepo.findOne(ticket);
        Usuario staff = staffService.findById(staffId);
        if (ticketModificada != null) {
            ticketModificada.assignTicket(staff);
            readTicketStaff(ticketModificada.getId(), true);
            readTicketUser(ticketModificada.getId(), true);
            return ticketRepo.save(ticketModificada);
        }
        return null;
    }

    private void readTicketStaff(Integer ticket, Boolean unread) {
        Ticket ticketModificada = ticketRepo.findOne(ticket);
        if (ticketModificada != null) {
            ticketModificada.staffReadTicket(unread);
            ticketRepo.save(ticketModificada);
        }
    }

    private void readTicketUser(Integer ticket, Boolean unread) {
        Ticket ticketModificada = ticketRepo.findOne(ticket);
        if (ticketModificada != null) {
            ticketModificada.setReadUnread(unread);
            ticketRepo.save(ticketModificada);
        }
    }

    public Ticket assignDepartment(Integer id, Departamento depa) {
        Ticket ticketModificada = ticketRepo.findOne(id);
        if (ticketModificada != null) {
            ticketModificada.changeDepartment(depa);
            readTicketStaff(ticketModificada.getId(), true);
            readTicketUser(ticketModificada.getId(), true);
            return ticketRepo.save(ticketModificada);
        }
        return null;
    }

    public Integer countNewTickets(Integer id) {
        return ticketRepo.countAllByDepartmentIdEqualsAndStaffIsNull(id);
    }

    public List<Ticket> getTicketsPorFiltros(Boolean nuevos, Boolean cerrados, Integer idStaff, Integer departamento) {

        if (cerrados && nuevos) { // Solo para debug, no deberia existir un ticket nuevo cerrado
            return ticketRepo.findAllByClosedEqualsAndStaffIsNullAndDepartmentIsNotNullOrderByDateAsc(true);
        }

        if (cerrados) { // Cerrados
            if (idStaff >= 0) {
                if (departamento >= 0) { // Cerrados (Por Usuario y Departamento)
                    return ticketRepo.findAllByClosedEqualsAndDepartmentIdEqualsAndStaffIdEqualsOrderByTicketNumberDesc(true, departamento, idStaff);
                } else { // Cerrados (Por Usuario)
                    return ticketRepo.findAllByClosedEqualsAndStaffIdEqualsOrderByTicketNumberDesc(true, idStaff);
                }
            } else {
                if (departamento >= 0) { // Cerrados (Por Departamento)
                    return ticketRepo.findAllByClosedEqualsAndDepartmentIdEqualsOrderByDateAsc(true, departamento);
                } else { // Cerrados (Todos)
                    return ticketRepo.findAllByClosedEqualsOrderByDateAsc(true);
                }
            }
        } else {
            if (nuevos) { // Nuevos

                if (departamento >= 0) {
                    return ticketRepo.findAllByClosedEqualsAndStaffIsNullAndDepartmentIdEqualsOrderByDateAsc(false, departamento);
                } else {
                    return ticketRepo.findAllByClosedEqualsAndStaffIsNullAndDepartmentIsNotNullOrderByDateAsc(false);
                }

            } else { // Sin Cerrar
                if (idStaff >= 0) {
                    if (departamento >= 0) { // Cerrados (Por Usuario y Departamento)
                        return ticketRepo.findAllByClosedEqualsAndDepartmentIdEqualsAndStaffIdEqualsOrderByTicketNumberDesc(false, departamento, idStaff);
                    } else { // Cerrados (Por Usuario)
                        return ticketRepo.findAllByClosedEqualsAndStaffIdEqualsOrderByTicketNumberDesc(false, idStaff);
                    }
                } else {
                    if (departamento >= 0) { // Cerrados (Por Departamento)
                        return ticketRepo.findAllByClosedEqualsAndDepartmentIdEqualsOrderByDateAsc(false, departamento);
                    } else { // Cerrados (Todos)
                        return ticketRepo.findAllByClosedEqualsOrderByDateAsc(false);
                    }
                }
            }
        }

    }

    public Integer getMaxTicketNumber() {
        return ticketRepo.getMaxTicketNumber();
    }

    public List<Ticket> getTicketStaff(Integer id) {
        List<Ticket> tickets = ticketRepo.findAllByStaffId(id);
        for (Ticket ticket : tickets) {
            if (ticket != null) {
                List<ActividadTicket> eventos = this.actividadTicketService.findAllByTicket(ticket.getId());
                ticket.setEvents(eventos);
            }
        }
        return tickets;
    }

    public Ticket getByTicketNumber(String tNumber, Integer idUser) {
        Ticket ticket = ticketRepo.findByTicketNumber(tNumber);
        readTicketUser(ticket.getId(), false);
        if (ticket.getUsuario().getId() == idUser) {
            return ticket;
        }
        return ticket;
    }

    public Ticket buildEventsAuthor(Ticket ticket) {
//        for (int j = 0; j < ticket.getEvents().size(); j++) {
//            ticket.getEvents().get(j).buildAuthor();
//            ticket.getEvents().get(j).setFechaString();
//            ticket.getEvents().get(j).setTicket(ticket.getId());
//            if (ticket.getEvents().get(j).getAuthor().getName() == null) {
//                ticket.getEvents().get(j).getAuthor().setName("");
//            }
//        }
        return ticket;
    }

    public List<Ticket> nuevos() {
        return ticketRepo.findAllByClosedEqualsAndStaffIsNullAndDepartmentIsNotNullOrderByDateAsc(false);
    }

    public List<Ticket> getTicketsStaffMemberOpen(Integer id, Boolean bol) {
        return ticketRepo.findAllByStaffIdAndClosed(id, bol);
    }

    public List<Ticket> getBusquedaGlobal(String ticket) {
        return ticketRepo.findAllByTicketNumberContainingOrContentContainingOrTitleContainingOrUsuario_NameContaining(ticket, ticket,ticket,ticket);
    }

    public void doActionListenerEnviarNotificacion(String titulo, String mensaje, String email, Ticket ticket) {
        try {
            Usuario usuario = usuarioService.findByUsuario(email);
            DataNotification dataNotification = new DataNotification();
            dataNotification.setClickAction(ClickAction.OPEN_TICKET_ACTIVITY.name());
            dataNotification.setTitulo(titulo);
            dataNotification.setMensaje(mensaje);
            dataNotification.setId(Integer.parseInt(ticket.getTicketNumber()));
            FirebaseResponse responseEntity;
            //TODO Mejorar implementacion  notificacion
            Notification notification = new Notification("default", titulo, mensaje, ClickAction.OPEN_TICKET_ACTIVITY.name());
            Push push = new Push(usuario.getDeviceId(), "high", notification, dataNotification);
            responseEntity = pushNotificationService.sendNotification(push);
            System.err.println("success: " + responseEntity.getSuccess());
            System.err.println("faiulure: " + responseEntity.getFailure());
            System.err.println("Results: " + responseEntity.getResults());

            if (responseEntity.getSuccess() == 1) {
                saveNotificacion(mensaje, titulo, ticket, usuario);
                System.err.println("Se envió la notificación");
            } else if (responseEntity.getFailure() == 1) {
                System.err.println("Falló al enviar notificación al dispositivo");
            }
        } catch (Exception e) {
        }

    }

    private void saveNotificacion(String mensajeNotificacion, String titulo, Ticket ticket, Usuario usuario) {
        Notificacion notificacion;
        Date date = new Date();

        notificacion = new Notificacion();
        notificacion.setUsuario(usuario);
        notificacion.setTitulo(titulo);
        notificacion.setMensaje(mensajeNotificacion);
        notificacion.setFechaCreacion(date);
        notificacion.setId(ticket.getId());
        try {
            notificacionService.save(notificacion);
            System.err.println("Se guardo el registro de la notificación");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
