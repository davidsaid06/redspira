package com.certuit.cercana.service.ticket;

import com.certuit.cercana.domain.entity.ActividadTicket;
import com.certuit.cercana.domain.entity.Ticket;
import com.certuit.cercana.domain.enums.TipoActividadTicket;
import com.certuit.cercana.repositories.ActividadTicketRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author Certuit
 */
@Service
public class ActividadTicketService extends ServiceBase<ActividadTicket, Integer> {

    @Autowired
    private ActividadTicketRepository repo;

    public ActividadTicketService(CrudRepository<ActividadTicket, Integer> repo) {
        super(repo);
    }

    /**
     * @param entity
     * @return
     */

    @Transactional
    public ActividadTicket save(ActividadTicket entity) {
        return this.repo.save(entity);
    }

    public List<ActividadTicket> findAll() {
        return this.repo.findAll();
    }

    public ActividadTicket findOne(Integer id) {
        return this.repo.findOne(id);
    }

    public List<ActividadTicket> findAllByTicket(int id) {
        return this.repo.findAllByTicketId(id);
    }

    public List<ActividadTicket> findAllByTicketAndTipoOrderByFechaActividad(int id, int tipo) {
        return this.repo.findAllByTicketIdAndTipoEqualsOrderByFechaActividadAsc(id, tipo);
    }

    public int countAllByTicketAndUsuarioAndTipo(int id, int usuario, int tipo) {
        return this.repo.countAllByTicketIdAndUsuarioIdAndTipo(id, usuario, tipo);
    }

    public void registrarAutoAsignar(Ticket ticket) {
        System.err.println("Se Auto-asignó a " + ticket.getStaff().getName());

        ActividadTicket actividadSolicitudInformacion = new ActividadTicket();
        Date date = new Date();

        actividadSolicitudInformacion.setTicket(ticket);
        //FIXME esta registrando al paciente en la autoasignación
        //actividadSolicitudInformacion.setStaff(ticket.getStaff());
        actividadSolicitudInformacion.setTipo(TipoActividadTicket.SISTEMA.getId()); //SISTEMA
        actividadSolicitudInformacion.setTitulo("Auto-asignación");
        actividadSolicitudInformacion.setMensaje("Se ha auto-asignado a " + ticket.getStaff().getName());
        //actividadSolicitudInformacion.setFechaActividad(new Date());

        try {
            this.repo.save(actividadSolicitudInformacion);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
