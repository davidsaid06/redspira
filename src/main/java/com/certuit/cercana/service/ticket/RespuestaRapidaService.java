package com.certuit.cercana.service.ticket;

import com.certuit.cercana.domain.entity.RespuestaRapida;
import com.certuit.cercana.repositories.RespuestaRapidaRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Certuit
 */
@Service
public class RespuestaRapidaService extends ServiceBase<RespuestaRapida, Integer> {

    @Autowired
    private RespuestaRapidaRepository repo;

    public RespuestaRapidaService(CrudRepository<RespuestaRapida, Integer> repo) {
        super(repo);
    }

    /**
     * @param entity
     * @return
     */
    @Transactional
    public RespuestaRapida save(RespuestaRapida entity) {
        return this.repo.save(entity);
    }

    public List<RespuestaRapida> findAll() {
        return this.repo.findAll();
    }

    public RespuestaRapida findOne(Integer id) {
        return this.repo.findOne(id);
    }

    public List<RespuestaRapida> findAllByVigenteOrderByNombreAsc(boolean vigente) {
        return this.repo.findByVigenteOrderByNombreAsc(vigente);
    }

    public List<RespuestaRapida> findAllByVigente(boolean vigente) {
        return this.repo.findByVigente(vigente);
    }

    public List<RespuestaRapida> findAllByNombreContainingAndVigenteOrderByNombreAsc(String nombre, boolean vigente) {
        return this.repo.findAllByNombreContainingAndVigenteOrderByNombreAsc(nombre, vigente);
    }






}
