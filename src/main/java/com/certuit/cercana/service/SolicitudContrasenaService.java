package com.certuit.cercana.service;

import com.certuit.cercana.domain.entity.SolicitudContrasena;
import com.certuit.cercana.repositories.SolicitudContrasenaRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Certuit
 */
@Service
public class SolicitudContrasenaService extends ServiceBase<SolicitudContrasena, Integer> {

    @Autowired
    private SolicitudContrasenaRepository repo;

    public SolicitudContrasenaService(CrudRepository<SolicitudContrasena, Integer> repo) {
        super(repo);
    }

    /**
     * @param entity
     * @return
     */
    @Transactional
    public SolicitudContrasena save(SolicitudContrasena entity) {
        return this.repo.save(entity);
    }

    public List<SolicitudContrasena> findAll() {
        return this.repo.findAll();
    }

    public SolicitudContrasena findOne(Integer id) {
        return this.repo.findOne(id);
    }

}
