package com.certuit.cercana.service;

import com.certuit.cercana.domain.entity.ParametroSistema;
import com.certuit.cercana.repositories.ParametroSistemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Certuit
 */
@Service
public class ParametroSistemaService {

    @Autowired
    private ParametroSistemaRepository repo;

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private String getByPropiedad(String propiedad) {
        ParametroSistema parametro = repo.findFirstByPropiedad(propiedad);
        if (parametro != null)
            return parametro.getValor();
        else
            return null;
    }

    private int setByPropiedad(String propiedad, String valor) {
        ParametroSistema parametro = repo.findFirstByPropiedad(propiedad);
        if (parametro != null) {
            parametro.setValor(valor);
            repo.save(parametro);
            return 0;
        } else {
            return 1;
        }
    }

//    public int getPeriodoPredeterminado() {
//        int periodo_id = 0;
//        try {
//            periodo_id = Integer.parseInt(getByPropiedad("periodo_predeterminado"));
//        } catch (Exception e) {
//            periodo_id = 0;
//        }
//        return periodo_id;
//    }

//    public int setPeriodoPredeterminado(int periodo_admisiones) {
//        return setByPropiedad("periodo_predeterminado", periodo_admisiones + "");
//    }

//    public int getPeriodoCaptura() {
//        int periodo_id = 0;
//        try {
//            periodo_id = Integer.parseInt(getByPropiedad("periodo_captura"));
//        } catch (Exception e) {
//            periodo_id = 0;
//        }
//        return periodo_id;
//    }

//    public int setPeriodoCaptura(int periodo_captura) {
//        return setByPropiedad("periodo_captura", periodo_captura + "");
//    }

//    public double getCalificacionMinimaAprobatoria() {
//        double calificacion = 0;
//        try {
//            calificacion = Double
//                    .parseDouble(getByPropiedad("calificacion_minima_aprobatoria"));
//        } catch (Exception e) {
//            calificacion = 6;
//        }
//        return calificacion;
//    }

//    public int setCalificacionMinimaAprobatoria(
//            double calificacion_minima_aprobatoria) {
//        return setByPropiedad("calificacion_minima_aprobatoria",
//                round(calificacion_minima_aprobatoria, 2) + "");
//    }

//    public int getPeriodoAdmisiones() {
//        int periodo_id = 0;
//        try {
//            periodo_id = Integer.parseInt(getByPropiedad("periodo_admisiones"));
//        } catch (Exception e) {
//            periodo_id = 0;
//        }
//        return periodo_id;
//    }

//    public int setPeriodoAdmisiones(int periodo_admisiones) {
//        return setByPropiedad("periodo_admisiones", periodo_admisiones + "");
//    }

//    public Date getFechaInicioCaptura() {
//        try {
//            Date fecha_limite = new SimpleDateFormat("dd/MM/yyyy HH:mm")
//                    .parse(getByPropiedad("fecha_inicio_captura"));
//            return fecha_limite;
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

//    public int setFechaInicioCaptura(Date fecha_inicio_captura) {
//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//        return setByPropiedad("fecha_inicio_captura",
//                df.format(fecha_inicio_captura));
//    }

//    public Date getFechaFinCaptura() {
//        try {
//            Date fecha = new SimpleDateFormat("dd/MM/yyyy HH:mm")
//                    .parse(getByPropiedad("fecha_fin_captura"));
//            return fecha;
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

//    public int setFechaFinCaptura(Date fecha_fin_captura) {
//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//        return setByPropiedad("fecha_fin_captura", df.format(fecha_fin_captura));
//    }

//    public String getClavePublicoGeneral() {
//        return getByPropiedad("clave_publico_general");
//    }

//    public int setClavePublicoGeneral(String clave_publico_general) {
//        return setByPropiedad("clave_publico_general", clave_publico_general.toUpperCase());
//    }

//    public Date getFechaLimitePagoFicha() {
//        Date fecha = null;
//        try {
//            fecha = new SimpleDateFormat("dd/MM/yyyy")
//                    .parse(getByPropiedad("fecha_limite_pago_ficha"));
//            return fecha;
//        } catch (ParseException e) {
//            fecha = new Date();
//            e.printStackTrace();
//        }
//        return fecha;
//    }

//    public int setFechaLimitePagoFicha(Date fecha_limite_pago_ficha) {
//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//        return setByPropiedad("fecha_limite_pago_ficha",
//                df.format(fecha_limite_pago_ficha));
//    }

//    public Date getFechaLimitePagoInscripcion() {
//        Date fecha = null;
//        try {
//            fecha = new SimpleDateFormat("dd/MM/yyyy")
//                    .parse(getByPropiedad("fecha_limite_pago_inscripcion"));
//            return fecha;
//        } catch (ParseException e) {
//            fecha = new Date();
//            e.printStackTrace();
//        }
//        return fecha;
//    }

//    public int setFechaLimitePagoInscripcion(Date fecha_limite_pago_inscripcion) {
//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//        return setByPropiedad("fecha_limite_pago_inscripcion",
//                df.format(fecha_limite_pago_inscripcion));
//    }

//    public double getComisionBancaria() {
//        double valor = Double.parseDouble(getByPropiedad("comision_bancaria"));
//        return round(valor, 2);
//    }

//    public int setComisionBancaria(double comision_bancaria) {
//        return setByPropiedad("comision_bancaria", round(comision_bancaria, 2)
//                + "");
//    }

//    public String getNombreDirector() {
//        return getByPropiedad("nombre_director");
//    }

//    public int setNombreDirector(String nombre_director) {
//        return setByPropiedad("nombre_director", nombre_director);
//    }

//    public int getDiasPagoConstancia() {
//        return Integer.parseInt(getByPropiedad("dias_pago_constancia"));
//    }

//    public int setDiasPagoConstancia(int dias_pago_constancia) {
//        return setByPropiedad("dias_pago_constancia", dias_pago_constancia + "");
//    }

//    public double getComisionBancariaConstancia() {
//        double valor = Double
//                .parseDouble(getByPropiedad("comision_bancaria_constancia"));
//        return round(valor, 2);
//    }

//    public int setComisionBancariaConstancia(double comision_bancaria_constancia) {
//        return setByPropiedad("comision_bancaria_constancia",
//                round(comision_bancaria_constancia, 2) + "");
//    }

//    public Date getFechaInicioInscripcionOnline() {
//        try {
//            Date fecha_limite = new SimpleDateFormat("dd/MM/yyyy HH:mm")
//                    .parse(getByPropiedad("fecha_inicio_inscripcion_online"));
//            return fecha_limite;
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

//    public int setFechaInicioInscripcionOnline(
//            Date fecha_inicio_inscripcion_online) {
//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//        return setByPropiedad("fecha_inicio_inscripcion_online",
//                df.format(fecha_inicio_inscripcion_online));
//    }

//    public Date getFechaFinInscripcionOnline() {
//        try {
//            Date fecha = new SimpleDateFormat("dd/MM/yyyy HH:mm")
//                    .parse(getByPropiedad("fecha_fin_inscripcion_online"));
//            return fecha;
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

//    public int setFechaFinInscripcionOnline(Date fecha_fin_inscripcion_online) {
//        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//        return setByPropiedad("fecha_fin_inscripcion_online",
//                df.format(fecha_fin_inscripcion_online));
//    }

    public String getServerUrl() {
        return getByPropiedad("server_url");
    }

    public String getPublicServerUrl() {
        return getByPropiedad("public_server_url");
    }

    public int setServerUrl(String serverUrl) {
        return setByPropiedad("server_url", serverUrl);
    }

    public String getCorreoSoporte() {
        return getByPropiedad("correo_soporte");
    }

    public int setCorreoSoporte(String correoSoporte) {
        return setByPropiedad("correo_soporte", correoSoporte);
    }

//    public boolean getEstatusServicioCurp() {
//        boolean estatus = false;
//        try {
//            estatus = Boolean.parseBoolean(getByPropiedad("servicio_curp_habilitado"));
//        } catch (Exception e) {
//            estatus = false;
//        }
//        return estatus;
//    }

//    public int setEstatusServicioCurp(Boolean estatus) {
//        return setByPropiedad("servicio_curp_habilitado", estatus + "");
//    }

//    public String getEnlaceCurp() {
//        return getByPropiedad("enlace_curp");
//    }

//    public int setEnlaceCurp(String enlace_curp) {
//        return setByPropiedad("enlace_curp", enlace_curp);
//    }

//    public int getEdadMinimaSEE() {
//        int edad_minima_see = 0;
//        try {
//            edad_minima_see = Integer.parseInt(getByPropiedad("edad_minima_see"));
//        } catch (Exception e) {
//            edad_minima_see = 15;
//        }
//        return edad_minima_see;
//    }

//    public int setEdadMinimaSEE(int edad_minima_see) {
//        return setByPropiedad("edad_minima_see", edad_minima_see + "");
//    }

//    public int getDecimalesCalificacionFinal() {
//        int decimales_calificacion_final = 0;
//        try {
//            decimales_calificacion_final = Integer.parseInt(getByPropiedad("decimales_calificacion_final"));
//        } catch (Exception e) {
//            decimales_calificacion_final = 0;
//        }
//        return decimales_calificacion_final;
//    }

//    public int setDecimalesCalificacionFinal(int decimales_calificacion_final) {
//        return setByPropiedad("decimales_calificacion_final", decimales_calificacion_final + "");
//    }


//    public boolean isEdicionPromedioHabilitada() {
//        boolean estatus = true;
//        try {
//            estatus = Boolean.parseBoolean(getByPropiedad("edicion_promedio_habilitado"));
//        } catch (Exception e) {
//            estatus = true;
//        }
//        return estatus;
//    }

//    public int setEdicionPromedioHabilitada(Boolean estatus) {
//        return setByPropiedad("edicion_promedio_habilitado", estatus + "");
//    }
//
//    public String getNombreEscuela() {
//        return getByPropiedad("escuela_see");
//    }
//
//    public int setNombreEscuela(String escuela_see) {
//        return setByPropiedad("escuela_see", escuela_see);
//    }
//
//    public int getHorasSemestrales() {
//        int horas_semestrales = 56;
//        try {
//            horas_semestrales = Integer.parseInt(getByPropiedad("horas_semestrales"));
//        } catch (Exception e) {
//            horas_semestrales = 0;
//        }
//        return horas_semestrales;
//    }
//
//    public int setHorasSemestrales(int horas_semestrales) {
//        return setByPropiedad("horas_semestrales", horas_semestrales + "");
//    }
//
//    public boolean isAccesoAlumnosHabilitado() {
//        boolean estatus = true;
//        try {
//            estatus = Boolean.parseBoolean(getByPropiedad("acceso_alumnos_habilitado"));
//        } catch (Exception e) {
//            estatus = false;
//        }
//        return estatus;
//    }
//
//    public int setAccesoAlumnosHabilitado(Boolean estatus) {
//        return setByPropiedad("acceso_alumnos_habilitado", estatus + "");
//    }
//
//    public boolean isWebServiceUabcHabilitado() {
//        boolean estatus = true;
//        try {
//            estatus = Boolean.parseBoolean(getByPropiedad("webservice_uabc_habilitado"));
//        } catch (Exception e) {
//            estatus = true;
//        }
//        return estatus;
//    }
//
//    public int setWebServiceUabcHabilitado(Boolean estatus) {
//        return setByPropiedad("webservice_uabc_habilitado", estatus + "");
//    }
//
}
