package com.certuit.cercana.service;

import com.certuit.cercana.domain.entity.Rol;
import com.certuit.cercana.domain.entity.Usuario;
import com.certuit.cercana.repositories.RolRepository;
import com.certuit.cercana.repositories.UsuarioRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioService extends ServiceBase<Usuario, Integer> implements UserDetailsService {

    @Autowired
    private UsuarioRepository repo;
    @Autowired
    private RolRepository rolRepository;

    public UsuarioService(CrudRepository<Usuario, Integer> repo) {
        super(repo);
    }

    @Override
    public UserDetails loadUserByUsername(String userName)
            throws UsernameNotFoundException {
        Usuario activeUserInfo = repo.findFirstByEmail(userName);
        List<Rol> roles = activeUserInfo.getRoles();
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Rol rol : roles) {
            GrantedAuthority authority = new SimpleGrantedAuthority(rol.getNombre());
            authorities.add(authority);
        }
        UserDetails userDetails = (UserDetails) new User(activeUserInfo.getUsername(),
                activeUserInfo.getPassword(), authorities);
        return userDetails;
    }

    public Usuario createUsuario(Usuario usuario) {
        return this.repo.save(usuario);
    }


    public Usuario findByUsuario(String username) {
        return this.repo.findFirstByEmail(username);
    }

    public Usuario findOne(Integer id) {
        return this.repo.findOne(id);
    }

    public Usuario save(Usuario usuario) {
        return this.repo.save(usuario);
    }

    public List<Usuario> findAll() {
        return this.repo.findAll();
    }

    public List<Usuario> findAllByRoles(List<Rol> roles, Boolean activo) {
        if (activo == null)
            return this.repo.findAllByRolesIn(roles);
        else
//            return this.repo.findAllByRolesInAndEnabled(roles, activo);
            return this.repo.findAllByRolesIn(roles);
    }

    public Usuario updateDeviceId(String username,String deviceId){
        Usuario user = this.repo.findFirstByEmail(username);
        user.setDeviceId(deviceId);
        this.save(user);
        return user;
    }




    public Usuario findFirstByFacebookId(String facebookId) {
        return this.repo.findFirstByFacebookId(facebookId);
    }
}
