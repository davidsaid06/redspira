package com.certuit.cercana.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;

/**
 * @author Certuit
 */
@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;
    @Getter
    @Setter
    private MimeMessage message;
    @Getter
    @Setter
    private String fromAddress = "pruebacorreo@certuit.com";

    public EmailService() {
    }

    public void sendMail(final String messageBody, final String subject,
                         final String toAddress) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("enviara correo a: " + toAddress);
                    message = mailSender.createMimeMessage();
                    MimeMessageHelper helper = new MimeMessageHelper(message,
                            true, "UTF-8");

                    helper.setFrom(fromAddress);
                    helper.setSubject(subject);
                    helper.setTo(toAddress);
                    //true para enviar texto enriquecido
                    helper.setText(messageBody, true);
                } catch (MessagingException me) {
                    me.printStackTrace();
                }
                try {
                    mailSender.send(message);
                } catch (MailAuthenticationException ex) {
                    ex.printStackTrace();

                } catch (MailException me) {
                    me.printStackTrace();
                }
            }
        }).start();
    }

    public void sendMail(final String messageBody, final String subject,
                         final String[] toAddresses) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    for (String toAddress : toAddresses) {
                        System.out.println("enviara correo a: " + toAddress);
                    }
                    message = mailSender.createMimeMessage();
                    MimeMessageHelper helper = new MimeMessageHelper(message,
                            true, "UTF-8");

                    helper.setFrom(fromAddress);
                    helper.setSubject(subject);
                    helper.setTo(toAddresses);
                    //true para enviar texto enriquecido
                    helper.setText(messageBody, true);
                } catch (MessagingException me) {
                    me.printStackTrace();
                }
                try {
                    mailSender.send(message);
                } catch (MailAuthenticationException ex) {
                    ex.printStackTrace();

                } catch (MailException me) {
                    me.printStackTrace();
                }
            }
        }).start();
    }

    public void sendMail(final String messageBody, final String subject,
                         final String[] toAddresses, List<File> attachments) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    for (String toAddress : toAddresses) {
                        System.out.println("enviara correo a: " + toAddress);
                    }
                    message = mailSender.createMimeMessage();
                    MimeMessageHelper helper = new MimeMessageHelper(message,
                            true, "UTF-8");

                    helper.setFrom(fromAddress);
                    helper.setSubject(subject);
                    helper.setTo(toAddresses);
                    //true para enviar texto enriquecido
                    helper.setText(messageBody, true);
                    for (File attachment : attachments) {
                        helper.addAttachment(attachment.getName(), attachment);
                        System.err.println(attachment.getName() + "sending...");

                    }
                } catch (MessagingException me) {
                    me.printStackTrace();
                }
                try {
                    mailSender.send(message);
                } catch (MailAuthenticationException ex) {
                    ex.printStackTrace();

                } catch (MailException me) {
                    me.printStackTrace();
                }
            }
        }).start();
    }

    public void sendMail(final String messageBody, final String subject,
                         final String toAddress, List<File> attachments) {
        {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        System.out.println("enviara correo a: " + toAddress);
                        message = mailSender.createMimeMessage();
                        MimeMessageHelper helper = new MimeMessageHelper(message,
                                true, "UTF-8");

                        helper.setFrom(fromAddress);
                        helper.setSubject(subject);
                        helper.setTo(toAddress);
                        //true para enviar texto enriquecido
                        helper.setText(messageBody, true);
                        // let's include the infamous windows Sample file (this time copied to c:/)
                        for (File attachment : attachments) {
                            helper.addAttachment(attachment.getName(), attachment);
                            System.err.println(attachment.getName() + "sending...");

                        }
                    } catch (MessagingException me) {
                        me.printStackTrace();
                    }
                    try {
                        mailSender.send(message);
                    } catch (MailAuthenticationException ex) {
                        ex.printStackTrace();

                    } catch (MailException me) {
                        me.printStackTrace();
                    }
                }
            }).start();
        }
    }

    public void sendMail(final String messageBody, final String subject,
                         final String toAddress, File attachment) {
        {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        System.out.println("enviara correo a: " + toAddress);
                        message = mailSender.createMimeMessage();
                        MimeMessageHelper helper = new MimeMessageHelper(message,
                                true, "UTF-8");

                        helper.setFrom(fromAddress);
                        helper.setSubject(subject);
                        helper.setTo(toAddress);
                        //true para enviar texto enriquecido
                        helper.setText(messageBody, true);
                        // let's include the infamous windows Sample file (this time copied to c:/)
//                       FileSystemResource file = new FileSystemResource(attachment);
                        helper.addAttachment(attachment.getName(), attachment);
                        System.err.println(attachment.getName());
                    } catch (MessagingException me) {
                        me.printStackTrace();
                    }
                    try {
                        mailSender.send(message);
                    } catch (MailAuthenticationException ex) {
                        ex.printStackTrace();

                    } catch (MailException me) {
                        me.printStackTrace();
                    }
                }
            }).start();
        }
    }
}



