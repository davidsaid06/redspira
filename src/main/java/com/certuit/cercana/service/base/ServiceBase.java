package com.certuit.cercana.service.base;

import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

public class ServiceBase<T, ID extends Serializable> {

    protected CrudRepository<T, ID> repo;

    public ServiceBase(CrudRepository<T, ID> repo) {
        this.repo = repo;
    }

    public <S extends T> S save(S entity) {
        return this.repo.save(entity);
    }

    public <S extends T> Iterable<S> save(Iterable<S> entity) {
        return this.repo.save(entity);
    }

    public T findOne(ID entity) {
        return this.repo.findOne(entity);
    }

    public boolean exists(ID entity) {
        return this.repo.exists(entity);
    }

    public Iterable<T> findAll() {
        return this.repo.findAll();
    }

    public Iterable<T> findAll(Iterable<ID> entity) {
        return this.repo.findAll(entity);
    }

    public long count() {
        return this.repo.count();
    }

    public void delete(ID entity) {
        this.repo.delete(entity);
    }

    public void delete(T entity) {
        this.repo.delete(entity);
    }

    public void delete(Iterable<? extends T> entity) {
        this.repo.delete(entity);
    }

    public void deleteAll() {
        this.repo.deleteAll();
    }
}

