package com.certuit.cercana.service;

import com.certuit.cercana.domain.entity.Acuerdo;
import com.certuit.cercana.repositories.AcuerdoRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Certuit
 */
@Service
public class AcuerdoService extends ServiceBase<Acuerdo, Integer> {

    @Autowired
    private AcuerdoRepository repo;

    public AcuerdoService(CrudRepository<Acuerdo, Integer> repo) {
        super(repo);
    }

    /**
     * @param entity
     * @return
     */
    @Transactional
    public Acuerdo save(Acuerdo entity) {
        return this.repo.save(entity);
    }

    public List<Acuerdo> findAll() {
        return this.repo.findAll();
    }

    public Acuerdo findOne(Integer id) {
        return this.repo.findOne(id);
    }

    public Acuerdo findByTipo(String tipo) {
        return this.repo.findByTipo(tipo);
    }

}
