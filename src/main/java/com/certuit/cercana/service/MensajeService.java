package com.certuit.cercana.service;

import com.certuit.cercana.domain.entity.Mensaje;
import com.certuit.cercana.repositories.MensajeRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Certuit
 */
@Service
public class MensajeService extends ServiceBase<Mensaje, Integer> {

    @Autowired
    private MensajeRepository repo;

    public MensajeService(CrudRepository<Mensaje, Integer> repo) {
        super(repo);
    }

    /**
     * @param entity
     * @return
     */
    @Transactional
    public Mensaje save(Mensaje entity) {
        return this.repo.save(entity);
    }

    public List<Mensaje> findAll() {
        return this.repo.findAll();
    }

    public Mensaje findOne(Integer id) {
        return this.repo.findOne(id);
    }


    private Mensaje convertirTags(Mensaje msgMail, String contenido, String asunto, String... tags) {
        final String[] TAGS = {"@responsable", "@nombre", "@uo", "@revisor"};
        String contenidoSinTags = contenido, asusntoSinTags = asunto;

        for (int i = 0; i < tags.length; i++) {
            contenidoSinTags = contenidoSinTags.replaceAll(TAGS[i], tags[i]);
            asusntoSinTags = asusntoSinTags.replaceAll(TAGS[i], tags[i]);
        }
        msgMail.setContenido(contenidoSinTags);
        msgMail.setAsunto(asusntoSinTags);

        return msgMail;
    }

    private Mensaje convertirTags(String contenido, String asunto, String... tags) {
        final String[] TAGS = {"@responsable", "@nombre", "@uo", "@revisor"};
        String contenidoSinTags = contenido, asusntoSinTags = asunto;

        for (int i = 0; i < tags.length; i++) {
            contenidoSinTags = contenidoSinTags.replaceAll(TAGS[i], tags[i]);
            asusntoSinTags = asusntoSinTags.replaceAll(TAGS[i], tags[i]);
        }

        Mensaje mensaje = new Mensaje(contenidoSinTags, asusntoSinTags);
        return mensaje;
    }

    public Mensaje getMensajeReformatted(String contenido, String descripcion, String... tags) {
        return convertirTags(contenido, descripcion, tags);
    }

    public Mensaje getMensajeByTipoNotificacion(int tipoNotificacion) {
        return repo.findFirstByTipoNotificacion(tipoNotificacion);
    }

    public Mensaje getMensajeById(int idMsgCorreo, String... tags) {
        Mensaje mensaje = repo.findOne(idMsgCorreo);
        if (mensaje != null) {
            return null;
        } else {
            return convertirTags(mensaje, mensaje.getContenido(), mensaje.getAsunto(), tags);
        }
    }


}
