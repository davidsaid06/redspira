package com.certuit.cercana.service;

import com.certuit.cercana.domain.entity.Notificacion;
import com.certuit.cercana.repositories.NotificacionRepository;
import com.certuit.cercana.service.base.ServiceBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Certuit
 */
@Service
public class NotificacionService extends ServiceBase<Notificacion, Integer> {

    @Autowired
    private NotificacionRepository repo;

    public NotificacionService(CrudRepository<Notificacion, Integer> repo) {
        super(repo);
    }

    /**
     * @param entity
     * @return
     */
    @Transactional
    public Notificacion save(Notificacion entity) {
        return this.repo.save(entity);
    }

    public List<Notificacion> findAll() {
        return this.repo.findAll();
    }

    public Notificacion findOne(Integer id) {
        return this.repo.findOne(id);
    }

    public List<Notificacion> findallByUsuarioOrderDesc(int id) {
        return this.repo.findByUsuario_IdOrderByFechaCreacionDesc(id);
    }

    public int countAllByUsuario(int id) {
        return this.repo.countAllByUsuario_Id(id);
    }


}
