package com.certuit.cercana.util;

import com.certuit.cercana.view.base.ApplicationBean;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPTable;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.IOUtils;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import java.io.*;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class JsfUtil extends ApplicationBean implements Serializable {

    private static final String RESORCES_FOLDER = "resources/main";
    private static final String IMAGE_FOLDER = "images";
    private static final String LOGOS_FOLDER = "logo";

    @Getter
    @Setter
    private String nombreReporte = "Reporte";

    public static SelectItem[] getSelectItems(List<?> entities, boolean selectOne) {
        int size = selectOne ? entities.size() + 1 : entities.size();
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        if (selectOne) {
            items[0] = new SelectItem("", "---");
            i++;
        }
        for (Object x : entities) {
            items[i++] = new SelectItem(x, x.toString());
        }
        return items;
    }

    public static void addErrorMessage(Exception ex, String defaultMsg) {
        String msg = ex.getLocalizedMessage();
        if (msg != null && msg.length() > 0) {
            addErrorMessage(msg);
        } else {
            addErrorMessage(defaultMsg);
        }
    }

    public static void addErrorMessages(List<String> messages) {
        for (String message : messages) {
            addErrorMessage(message);
        }
    }

    public static void addErrorMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static void addErrorMessage(String msg, String detail) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, detail);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static void addSuccessMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
    }

    public static void addSuccessMessage(String msg, String detail) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, detail);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
    }

    public static String getRequestParameter(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }

    public static Object getObjectFromRequestParameter(String requestParameterName, Converter converter,
                                                       UIComponent component) {
        String theId = JsfUtil.getRequestParameter(requestParameterName);
        return converter.getAsObject(FacesContext.getCurrentInstance(), component, theId);
    }

    public void postProcessXLS(Object document) {
        FileInputStream in = null;
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        //Definir estilo de celda
        HSSFRow header = sheet.getRow(0);
        HSSFFont font = wb.createFont();
        font.setColor(HSSFColor.WHITE.index);
        font.setBoldweight((short) 1);
        HSSFCellStyle cellStyle = wb.createCellStyle();
        HSSFPalette palette = ((HSSFWorkbook) wb).getCustomPalette();
        palette.setColorAtIndex((short) 57, (byte) 51, (byte) 175, (byte) 222);//Definir color custom
        cellStyle.setFillForegroundColor(palette.getColor(57).getIndex());
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        cellStyle.setFillBackgroundColor(HSSFColor.WHITE.index);
        cellStyle.setFont(font);
        //Agregar estilo a celdas
        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            header.getCell(i).setCellStyle(cellStyle);
            sheet.autoSizeColumn(i);//Ajustar ancho de celda automaticamente
        }
        //Insertar x rows
        for (int x = 0; x < 6; x++) {
            int createNewRowAt = 0; //Punto de partida para insertar rows
            sheet.shiftRows(createNewRowAt, sheet.getLastRowNum(), 1, true, false);
            HSSFRow newRow;
            newRow = sheet.createRow(createNewRowAt);
            newRow = sheet.getRow(createNewRowAt);
        }

        //Agregar titulo de reporte
        HSSFFont fontTitle = wb.createFont();
        fontTitle.setBoldweight((short) 1);
        fontTitle.setFontHeightInPoints((short) 20);
        HSSFCellStyle titleStyle = wb.createCellStyle();
        titleStyle.setFont(fontTitle);
        Row row = sheet.createRow((short) 4);
        Cell cell = row.createCell(0);
        cell.setCellValue(nombreReporte);
        cell.setCellStyle(titleStyle);

        //Agregar logo
        try {
            in = new FileInputStream(this.getAbsolutePath("logo.png"));
            byte[] bytes = IOUtils.toByteArray(in);
            int pictureIdx = wb.addPicture(bytes, wb.PICTURE_TYPE_PNG);
            CreationHelper helper = wb.getCreationHelper();
            Drawing drawing = sheet.createDrawingPatriarch();
            ClientAnchor anchor = helper.createClientAnchor();
            Picture picture = drawing.createPicture(anchor, pictureIdx);
            picture.resize(Double.MAX_VALUE);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void postProcessXLS2(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow header = sheet.getRow(0);

        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.GREEN.index);
//		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            HSSFCell cell = header.getCell(i);

            cell.setCellStyle(cellStyle);
        }
    }

    public void postProcessXLSOficial(Object document) {
        HSSFWorkbook wb = (HSSFWorkbook) document;
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow header = sheet.getRow(0);

        HSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.GREEN.index);
//		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        for (int i = 0; i < header.getPhysicalNumberOfCells(); i++) {
            HSSFCell cell = header.getCell(i);

            cell.setCellStyle(cellStyle);
        }
    }

    public void preProcessPDF(Object document) throws IOException, DocumentException {

        final Document pdf = (Document) document;

        pdf.setPageSize(PageSize.A4);
        pdf.open();

        PdfPTable pdfTable = new PdfPTable(1);
        pdfTable.addCell(getImage("logo.png"));

        //pdfTable.setWidthPercentage(60f);
        //pdfTable.setHorizontalAlignment(1);
        pdf.add(getImage("logo.png"));

        pdf.add(new Paragraph(this.nombreReporte,
                FontFactory.getFont(FontFactory.HELVETICA, 18, Font.BOLD, java.awt.Color.WHITE)));
        pdf.add(Chunk.NEWLINE);

        //SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        //pdf.add(new Phrase("Fecha: " + formato.format(new Date())));

    }

    public void postProcessPDF(Object document) throws IOException, DocumentException {
        final Document pdf = (Document) document;
        pdf.setPageSize(PageSize.A4.rotate());

    }

    private Image getImage(String imageName) throws IOException, BadElementException {
        final Image image = Image.getInstance(getAbsolutePath(imageName));
        image.scalePercent(90f);
        return image;
    }

    private String getAbsolutePath(String imageName) {
        final ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
                .getContext();
        final StringBuilder logo = new StringBuilder().append(servletContext.getRealPath(""));
        logo.append(File.separator).append(RESORCES_FOLDER);
        logo.append(File.separator).append(IMAGE_FOLDER);
        logo.append(File.separator).append(imageName);
        System.out.println(logo);
        return logo.toString();
    }

    public static String getShortLongDateFormat(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yy hh:mm:ss a");
        return sdf.format(date);

    }

    public static String getDateFormat(Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        if (date == null) {
            return "";
        }
        return sdf.format(date);

    }

    public static String getFileSize(long fileSize) {
        DecimalFormat df = new DecimalFormat("#.##");
        if (fileSize > 1024 * 1024) {
            return df.format(fileSize / (1024 * 1024)) + " MB";
        } else if (fileSize > 1024) {
            return df.format(fileSize / (1024)) + " KB";
        }

        return df.format(fileSize) + " B";
    }

    public static String saveFile(String fileName, String path, InputStream in) {
        String url = "";
        try {
            // write the inputStream to a FileOutputStream
            File file = new File(getDestination() + path + fileName);
            OutputStream out = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[2048];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
            url = getPublicUrl() + path + fileName;
            //change permission to 644
            file.setReadable(true, false);
            System.out.println("New file created!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return url;
    }


    public static String getTiempoTranscurrido(Date date) {
        String tiempoTanscurrido = "";
        TimeAgo time = new TimeAgo();

        if (date != null) {
            tiempoTanscurrido = time.timeAgo(date);
        }


        return tiempoTanscurrido;
    }

    public static Date getDateWithoutTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getFullDayDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static Date getDateTime(Date date, int hourOfDay, int minute, int second, int millisecond) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, millisecond);
        return calendar.getTime();
    }

    public static Date getDateTime(Date date, int dayOfMonth, int hourOfDay, int minute, int second, int millisecond) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, dayOfMonth);
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, millisecond);
        return calendar.getTime();
    }

    public static Date getDateTime(Date date, Time hora) {
        if (date != null && hora != null) {
            Calendar calendar = Calendar.getInstance();
            LocalTime localTime = LocalTime.parse(hora.toString());
            LocalDate localDate = new java.sql.Date(date.getTime()).toLocalDate();

            calendar.set(localDate.getYear(), localDate.getMonthValue() - 1, localDate.getDayOfMonth(), localTime.getHour(), localTime.getMinute(), 0);
            return calendar.getTime();
        }
        return null;
    }

    public static String getDateNumbers(Date date) {
        String dateNumbers = "";
        if (date != null) {
            return getDateFormat(date, "yyyy-MM-dd").replace("-", "");

        }

        return dateNumbers;
    }

    public static Time getTime(Date date) {
        Time time = new Time(date.getTime());
        return time;
    }

    public String getFechaDoubleAString(Double date) {
        String str = "0000-00-00 00:00";
        String minutos = "00";
        if (date != null) {
            try {
                if (date.toString().replace(".", "").length() >= 8) {
                    str = date.toString().replace(".", "");
                    str = str.replace("E", "0");
                    String ano = str.substring(0, 4);
                    String mes = str.substring(4, 6);
                    String dia = str.substring(6, 8);
                    String hora = str.substring(8, 10);
                    if (date.toString().replace(".", "").length() >= 12) {
                        minutos = str.substring(10, 12);
                    }
                    str = ano + "-" + mes + "-" + dia + " " + hora + ":" + minutos;
                    str = str.replace("E", "0");

                }
            } catch (NullPointerException e) {
                return str;
            }
        }
        return str;
    }

    public String getDateTimeDoubleAString(Double date) {
        String str = "0000-00-00 00:00";
        try {
            if (date.toString().replace(".", "").length() >= 8) {
                str = date.toString().replace(".", "");
                String ano = str.substring(0, 4);
                String mes = str.substring(4, 6);
                String dia = str.substring(6, 8);
                String hora = str.substring(8, 10);
                String minutos = str.substring(10, 12);
                str = ano + "-" + mes + "-" + dia + " " + hora + ":" + minutos;
                str.replace("E", "0");
            }
        } catch (NullPointerException e) {
            return str;
        }

        return str;
    }

    public String movilDate(Double date) {
        String str = getFechaDoubleAString(date);
        str = str.replace(" ", "");
        str = str.replace("-", "");
        str = str.replace(":", "");
        return str;
    }

    public static String getFormatNextTicket(int current_ticket_id) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
        String datetime = sdf.format(date);
        int ticket_number = current_ticket_id + 1;
        String ticketNumber = datetime.substring(0, 4);
        ticketNumber = ticketNumber + ticket_number;
        return ticketNumber;
    }

}
