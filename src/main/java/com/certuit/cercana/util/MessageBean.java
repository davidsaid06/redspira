package com.certuit.cercana.util;

import com.certuit.cercana.view.base.ApplicationBean;

import javax.faces.context.FacesContext;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class MessageBean extends ApplicationBean {

	private ResourceBundle bundleMessages;
	private ResourceBundle bundleParameters;
	private ResourceBundle bundleHelp;

	public ResourceBundle getBundleMessages() {
		if (bundleMessages == null) {
			FacesContext context = super.getFacesContext();
			bundleMessages = context.getApplication().getResourceBundle(
					context, "msg");
		}
		return bundleMessages;
	}

	public ResourceBundle getBundleParameters() {
		if (bundleParameters == null) {
			FacesContext context = super.getFacesContext();
			bundleParameters = context.getApplication().getResourceBundle(
					context, "app");
		}
		return bundleParameters;
	}

	public ResourceBundle getBundleHelp() {
		if (bundleHelp == null) {
			FacesContext context = super.getFacesContext();
			bundleHelp = context.getApplication().getResourceBundle(context,
					"help");
		}
		return bundleHelp;
	}



	public String getValue(String key) {
		String result = null;
		try {
			result = getBundleMessages().getString(key);
		} catch (MissingResourceException e) {
			result = "???" + key + "??? not found";
		}
		return result;
	}

	public String getParam(String key) {
		String result = null;
		try {
			result = getBundleParameters().getString(key);
		} catch (MissingResourceException e) {
			result = "???" + key + "??? not found";
		}
		return result;
	}

	public String getHelp(String key) {
		String result = null;
		try {
			result = getBundleHelp().getString(key);
		} catch (MissingResourceException e) {
			result = "???" + key + "??? not found";
		}
		return result;
	}

	/**
	 * Get string for key
	 *
	 * @param key
	 * @return string
	 */
	public String getString(String key) {
		try {
			return getBundleMessages().getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}

}