package com.certuit.cercana.util.curp;

public enum Sexo {

    HOMBRE("H"), MUJER("M");

    public String caracter;

    Sexo(String caracter) {
        this.caracter = caracter;
    }

}
