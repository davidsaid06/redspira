package com.certuit.cercana.util.curp;

public class CURPGenerator {

    public static String generar(String nombres, String apellidoPaterno, String apellidoMaterno, String ano, String mes,
            String dia, Sexo sexo, Entidad entidad) {
        StringBuilder curpStringBuilder = new StringBuilder();

        // Convertir todo a mayúsculas
        nombres = limpiar(nombres);
        apellidoPaterno = limpiar(apellidoPaterno);
        apellidoMaterno = limpiar(apellidoMaterno);

        apellidoPaterno = limpiarApellido(apellidoPaterno);
        apellidoMaterno = limpiarApellido(apellidoMaterno);

        // Corregir mes y año en caso de tener un sólo dígito
        mes = corregirLongitud(mes);
        dia = corregirLongitud(dia);

        // Letra inicial del primer apellido
        curpStringBuilder.append(ExpresionesRegulares.primeraLetra(apellidoPaterno))
                // primera vocal interna del primer apellido
                .append(ExpresionesRegulares.primeraVocalInterna(apellidoPaterno))
                // primera letra del segundo apellido
                .append(ExpresionesRegulares.primeraLetra(apellidoMaterno))
                // primera letra del nombre
                .append(ExpresionesRegulares.primeraLetra(nombres));

        // Se hacen las validaciones hasta este punto
        curpStringBuilder = ExpresionesRegulares.reemplazarCaracteresEspeciales(curpStringBuilder);
        curpStringBuilder = eliminarPalabrasAltisonantes(curpStringBuilder);

        // dos dígitos para el año
        curpStringBuilder.append(ano.substring(2, 4))
                // dos dígitos para el mes
                .append(mes)
                // dos dígitos para el día
                .append(dia)
                // caracter de sexo
                .append(sexo.caracter)
                // código de entidad
                .append(entidad.clave)
                // primera consonante interna del primer apellido
                .append(ExpresionesRegulares.primerConsonanteInterna(apellidoPaterno))
                // primera consonante interna del segundo apellido
                .append(ExpresionesRegulares.primerConsonanteInterna(apellidoMaterno))
                // primera consonante interna del nombre
                .append(ExpresionesRegulares.primerConsonanteInterna(nombres))
                // identificador de siglo
                .append(identificadorSiglo(ano))
                // dígito verificador
                .append(CalculadorDigitoVerificador.calcularDigitoVerificador(curpStringBuilder));

        return curpStringBuilder.toString();
    }

    private static String limpiarApellido(String apellido) {

        if (apellido.isEmpty()) {
            return apellido;
        }

        if (apellido.charAt(0) == 'Ñ') {
            apellido = apellido.replaceFirst("Ñ", "X");
        }
        return apellido;
    }

    private static String identificadorSiglo(String ano) {
        String siglo = ano.substring(0, 2);

        switch (siglo) {
        case "19":
            return "0";
        case "20":
            return "A";
        default:
            return null;
        }
    }

    private static String corregirLongitud(String valor) {
        if (valor.length() == 2) {
            return valor;
        }

        return "0" + valor;
    }

    private static String limpiar(String palabra) {
        // Se convierte a mayúsculas
        palabra = palabra.toUpperCase();

        // Se eliminan acentos y diéresis
        palabra = palabra.replaceAll("Á", "A");
        palabra = palabra.replaceAll("É", "E");
        palabra = palabra.replaceAll("Í", "I");
        palabra = palabra.replaceAll("Ó", "O");
        palabra = palabra.replaceAll("(?:Ú|Ü)", "U");

        String[] componentesPalabra = palabra.split("\\s");

        if (componentesPalabra.length == 1) {
            return componentesPalabra[0];
        }

        for (String nombre : componentesPalabra) {
            if (!Catalogos.LISTA_PRIMEROS_NOMBRES_IGNORADOS.contains(nombre)) {
                return nombre;
            }
        }

        return null;

    }

    private static StringBuilder eliminarPalabrasAltisonantes(StringBuilder palabra) {

        if (Catalogos.LISTA_PALABRAS_IMPROPIAS.contains(palabra.toString())) {
            return palabra.replace(1, 2, ExpresionesRegulares.CARACTER_DEFAULT);
        } else {
            return palabra;
        }

    }

}
