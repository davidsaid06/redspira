package com.certuit.cercana.util;
import com.certuit.cercana.domain.entity.Ruta;
import com.certuit.cercana.service.recoleccion.RutaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class Cron {

    //Ejemplos
// * "0 0 * * * *" = the top of every hour of every day.
// * "*/10 * * * * *" = every ten seconds.
// * "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
// * "0 0 8,10 * * *" = 8 and 10 o'clock of every day.
// * "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30 and 10 o'clock every day.
// * "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
// * "0 0 0 25 12 ?" = every Christmas Day at midnight
    public Cron() {

    }

    @Autowired
    RutaService rutaService;

    // 0 * * ? * *
    // 0 0 1 ? * * Todos los dias a la 1
    @Scheduled(cron = "0 0 1 ? * *")
    public void async() {
        List<Ruta> rutas = rutaService.findAll();
        for (Ruta ruta : rutas) {
            rutaService.clearObservaciones(ruta.getId());
            rutaService.changeStatus(ruta.getId(), 0);
        }
        System.err.println("Se ejecuto cron. Hora actual: " + new Date().toString());
    }

// cron = segundos, minutos, horas, dia del mes, mes, dia de la semana
// * means match any
// */X means "every X"
// ? ("no specific value") - useful when you need to specify something in one of the two fields in which the character is allowed

}
